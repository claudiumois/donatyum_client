import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import DoctorCard from './DoctorCard.jsx';

class Doctors extends Component {
    constructor() {
        super();
        this.state = {
            doctor: [],
        }
    }

    componentDidMount() {/*GET ALL Doctors */
        const apiUrl = 'http://localhost:8080/api/staff/doctors/find';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        doctor: result
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )

    }

    render() {
        let doctorCards = this.state.doctor.map(doctor => {
            return (
                <Col sm="4">
                    <DoctorCard key={doctor.idStaff} doctor={doctor} />
                </Col>
            )
        })
        return (
            <Container fluid className="back-color-staff">
                <Row>
                    {doctorCards}
                </Row>
                
            </Container>
        )
    }
}

export default Doctors;