import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import AssistentCard from './AssistentCard.jsx';

class Assistents extends Component {
    constructor() {
        super();
        this.state = {
            assistent: [],
        }
    }

    componentDidMount() {/*GET ALL Assistents */
        const apiUrl = 'http://localhost:8080/api/staff/asistents/find';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        assistent: result
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )

    }

    render() {
        let assistentCards = this.state.assistent.map(assistent => {
            return (
                <Col sm="4">
                    <AssistentCard key={assistent.idStaff} assistent={assistent} />
                </Col>
            )
        })
        return (
            <Container fluid className="back-color-staff">
                <Row>
                    {assistentCards}
                </Row>
                
            </Container>
        )
    }
}

export default Assistents;