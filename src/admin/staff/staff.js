import React from 'react';
import Modal from 'react-modal';
import Modal2 from 'react-modal';
import ModalConfirmDelete from '../../components/ModalConfirmDelete';
import ModalConfirmPassword from '../../components/ModalConfirmPassword';
import HasDeleted from '../../components/HasDeleted';
import HasCreated from '../../components/HasCreated';
import HasEditated from '../../components/HasEditated';
import { Button } from 'react-bootstrap';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import DatePicker from "react-datepicker";
import NoMatchPassword from '../../components/NoMatchPassword';
import NoName from '../../components/validateFields/NoName';
import NoUsername from '../../components/validateFields/NoUsername';
import NoEmail from '../../components/validateFields/NoEmail';
import NoPassword from '../../components/validateFields/NoPassword';



import '../staff/style.css';
import "react-datepicker/dist/react-datepicker.css";


class Staff extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            staffs: [],

            userName: '',
            password: '',
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            confirmPassword: '',
            phone: '',
            address: '',
            salary: '',
            staffOcupationId: '',
            birthDate: new Date(),
            gender: '',

            /*For Remember field in edit */
            userNameE: '',
            passwordE: '',
            firstNameE: '',
            lastNameE: '',
            emailE: '',
            passwordE: '',
            confirmPasswordE: '',
            phoneE: '',
            addressE: '',
            salaryE: '',
            staffOcupationIdE: '',
            birthDateE: new Date(),
            genderE: '',

            rememberId: 0,
            rememberIdForDeleteStaff: 0,

            errorMessages: '',
            errorUsername: '',
            errorName: '',
            errorEmail: '',
            errorPassword: '',

            successAction: '',
            open: false,
            open2: false,
            isActive: false,
            isActive2: false, /*For Edit */
            showModalConfirmDelete: false, /*Confirm delete modal */
            showModalConfirmPassword: false, /*Confirm password modal */
            ifChangePassword: false,/*If change password with confirmation password in edit */

        }

        this.onChange=this.onChange.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
    }


    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleClose = () => {
        this.setState({ open: false })
    };

    handleClose2 = () => {
        this.setState({ open2: false })
    };

    handleCloseModalConfirmDelete = () => {
        this.setState({ showModalConfirmDelete: false })
    };

    handleCloseModalConfirmPassword = () => {
        this.setState({ showModalConfirmPassword: false })
    };

    handleOpenModalConfirmDelete = () => {
        this.setState({ showModalConfirmDelete: true })
    };

    handleOpenModalConfirmPassword = () => {
        this.setState({ showModalConfirmPassword: true })
    };

    handleOpen = () => {
        this.setState({ open: true })
    };

    handleOpen2 = () => {
        this.setState({ open2: true })
    };

    handleChange = (event) => {
        this.setState({ staffOcupationId: event.target.value })
    };

    handleChange2 = (event) => {
        this.setState({ gender: event.target.value })
    };

    onChangeBirthDate = birthDate => this.setState({ birthDate })

    /* Pentru Edit. Atunci cand se face edit se face pe fiecare field imparte */

    handleChangeFirstName = (e) => {
        this.setState({
            firstName: e.target.value,
        });
    }

    handleChangeLastName = (e) => {
        this.setState({
            lastName: e.target.value,
        });
    }

    handleChangeUsername = (e) => {
        this.setState({
            userName: e.target.value,
        });
    }

    handleChangeEmail = (e) => {
        this.setState({
            email: e.target.value,
        });
    }

    handleChangeAddress = (e) => {
        this.setState({
            address: e.target.value,
        });
    }

    handleChangeUserOcupation = (e) => {
        this.setState({
            staffOcupationId: e.target.value,
        });
    }

    handleChangePhone = (e) => {
        this.setState({
            phone: e.target.value,
        });
    }


    handleChangePassword = (e) => {
        this.setState({
            password: e.target.value,

        });
    }

    handleChangeConfirmPassword = (e) => {
        this.setState({
            confirmPassword: e.target.value,
            ifChangePassword: true,
        });
    }

    handleChangeSalary = (e) => {
        this.setState({
            salary: e.target.value,
        });
    }

    /***************************************************************************/


    componentDidMount() {/*GET ALL Staffs */
        const apiUrl = 'http://localhost:8080/api/staff/find';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        staffs: result
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )

    }

    deleteStaff = () => {
        const apiUrl = 'http://localhost:8080/api/staff/delete/' + this.state.rememberIdForDeleteStaff;

        console.log(apiUrl);

        const options = {
            method: 'DELETE',
        }

        fetch(apiUrl, options)
            .then(this.setState({
                successAction: 'deleted',
            }))

        setTimeout(function () {
            window.location.reload(false);
        }.bind(this), 500)

    }

    handleSubmitEdit = (e) => {
        e.preventDefault();

        const editStaff = {
            userName: this.state.userName,
            password: this.state.password,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            phone: this.state.phone,
            birthDate: this.state.birthDate,
            address: this.state.address,
            salary: this.state.salary,
            gender: this.state.gender,
            staffOcupationId: this.state.staffOcupationId
        }

        console.log('password=' + this.state.password);
        console.log(' confirm pass=' + this.state.confirmPassword)

        if (this.state.password != this.state.confirmPassword && this.state.ifChangePassword == true) {
            this.setState({
                errorMessages: 'passwordNotCorectly',
            })
        } else {
            this.setState({
                errorMessages: '',
                successAction: 'editated',
                showModalConfirmPassword: false,
            })
            const apiUrl = "http://localhost:8080/api/staff/update/" + this.state.rememberId;

            const options = {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(editStaff)
            }

            fetch(apiUrl, options)
                .then(this.setState({
                    successAction: 'editated'
                }))
        }


    }

    roleStaff = (ocupationId) => {

        if (ocupationId == 1) {
            return "Admin";
        } else if (ocupationId == 2) {
            return "Doctor";
        } else if (ocupationId == 3) {
            return "Asistent"
        }
    }

    componentWillMount() {
        Modal.setAppElement('body');
    }

    toggleModalCreateStaff = () => {
        this.setState({
            isActive: !this.state.isActive,
            errorMessages: '',
            errorEmail: '',
            errorName: '',
            errorPassword: '',
            errorUsername: '',
        })
    }

    componentWillMount() {
        Modal2.setAppElement('body');
    }


    toggleModal2 = () => {
        this.setState({
            isActive2: !this.state.isActive2
        })

        console.log(this.state.rememberId)
    }


    onSubmit(e) {
        e.preventDefault()

        const newStaff = {
            userName: this.state.userName,
            password: this.state.password,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            phone: this.state.phone,
            address: this.state.address,
            birthDate: this.state.birthDate,
            salary: this.state.salary,
            gender: this.state.gender,
            staffOcupationId: this.state.staffOcupationId,
        }

        if (this.state.password != this.state.confirmPassword) {
            this.setState({
                errorMessages: 'passwordNotCorectly',
            })
        } else if (this.state.password == this.state.confirmPassword) {
            this.setState({
                errorMessages: '',
            })
        }

        if (this.state.firstName == '' && this.state.lastName == '') {
            this.setState({
                errorName: 'errorName'
            })
        } else {
            this.setState({
                errorName: ''
            })
        }

        if (this.state.userName == '') {
            this.setState({
                errorUsername: 'errorUsername'
            })
        } else {
            this.setState({
                errorUsername: ''
            })
        }

        if (this.state.email == '') {
            this.setState({
                errorEmail: 'errorEmail'
            })
        } else {
            this.setState({
                errorEmail: ''
            })
        }

        if (this.state.password == '') {
            this.setState({
                errorPassword: 'errorPassword'
            })
        } else {
            this.setState({
                errorPassword: ''
            })
        }

        if (this.state.password != '' && this.state.password == this.state.confirmPassword && this.state.confirmPassword != '' && (this.state.firstName != '' || this.state.lastName != '') && this.state.userName!= '' ){
                const options = {
                method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newStaff)

            }
            fetch('http://localhost:8080/api/staff/create', options)
                .then(this.setState({
                    successAction: "created",
                }))

            this.toggleModalCreateStaff();

            setTimeout(function () {
                window.location.reload(false); //asta imi da refres dar nu se mai vede record-ul
            }.bind(this), 500)
        }

    }

    rememberIdStaff = (id) => {
        this.setState({
            rememberId: id,
        })

        const apiUrl = 'http://localhost:8080/api/staff/find/' + id;

        console.log(apiUrl)
        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        firstName: result.firstName,
                        lastName: result.lastName,
                        userName: result.userName,
                        email: result.email,
                        phone: result.phone,
                        address: result.address,
                        birthDate: Date.parse(result.birthDate),
                        gender: result.gender,
                        salary: result.salary,
                        password: result.password,
                        staffOcupationId: result.staffOcupationId
                    });
                    console.log(result.gender)
                },
                (error) => {
                    this.setState({ error });
                }
            )

        this.toggleModal2();
    }

    confirmDelete = (id) => {
        this.setState({
            showModalConfirmDelete: true, /*Se deschide Modal-ul de confirmare stergere */
            rememberIdForDeleteStaff: id, /*Tine minte id-ul Stafului cand apasam pe Delete */
        })
        console.log(id)
    }

    confirmPassword = () => {
        this.setState({
            showModalConfirmPassword: true, /*Se deschide Modal-ul de confirmare password */
        })

    }

    closeAndSaveEdit = () => {
        this.toggleModal2();
        window.location.reload(false);
    }

    render() {


        const { staffs } = this.state;

        return (

            <div className="back-color">
                {this.state.successAction == "deleted" ? <HasDeleted /> : null}
                {this.state.successAction == "created" ? <HasCreated /> : null}
                {this.state.successAction == 'editated' ? <HasEditated /> : null}
                <hr />
                <h1 id='title'>Personal</h1>

                <table id='students'>
                    <thead>
                        <tr>
                            <th>Nume</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Parola</th>
                            <th>Zi de nastere</th>
                            <th>Sex</th>
                            <th>Adresa</th>
                            <th>Numar de telefon</th>
                            <th>Salariu</th>
                            <th>Rol</th>
                            <th>Optiune</th>
                        </tr>
                    </thead>
                    <tbody>
                        {staffs.map(staff => (
                            <tr key={staff.idStaff}>

                                <td>{staff.firstName} {staff.lastName}</td>
                                <td>{staff.userName}</td>
                                <td>{staff.email}</td>
                                <td>{staff.password}</td>
                                <td>{staff.birthDate}</td>
                                <td>{staff.gender}</td>
                                <td>{staff.address}</td>
                                <td>0{staff.phone}</td>
                                <td>{staff.salary}</td>
                                <td>{this.roleStaff(staff.staffOcupationId)}</td>
                                <td>
                                    <Button id="button-style" variant="danger" onClick={() => { this.confirmDelete(staff.idStaff) }} class="btn btn-danger">Sterge</Button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <Button id="button-style" variant="success" onClick={() => { this.rememberIdStaff(staff.idStaff) }} class="success">Editare</Button>
                                </td>

                            </tr>
                        ))}
                    </tbody>
                </table>

                <Modal
                    className="white"
                    size="lg"
                    overlayClassName="Overlay"
                    aria-labelledby="example-modal-sizes-title-sm" isOpen={this.state.isActive} onRequestClose={this.toggleModalCreateStaff}>
                    <Button id="button-style" type="submit" onClick={this.toggleModalCreateStaff} className="btn btn-secondary">Close</Button>


                    <div >

                        <div className="row">
                            <div className="col-md-6 mt-5 mx-auto">
                                <form noValidate onSubmit={this.onSubmit}>
                                    <h1 className="h3 mb-3 front-weight-normal">
                                        Creare cont
                                    </h1>


                                    <div>{this.state.errorName == 'errorName' ? <NoName /> : null}</div>
                                    <div className="form-group">
                                        <label htmlFor="firstName"><h6>Nume (*)</h6></label>
                                        <input type="text"
                                            className="form-control"
                                            name="firstName"
                                            placeholder="Enter First Name"
                                            value={this.state.firstName}
                                            onChange={this.onChange} />
                                    </div>

                                    <div>{this.state.errorName == 'errorName' ? <NoName /> : null}</div>
                                    <div className="form-group">
                                        <label htmlFor="lastName"><h6>Prenume (*)</h6></label>
                                        <input type="text"
                                            className="form-control"
                                            name="lastName"
                                            placeholder="Enter Last Name"
                                            value={this.state.lastName}
                                            onChange={this.onChange} />
                                    </div>


                                    <div>{this.state.errorUsername == 'errorUsername' ? <NoUsername /> : null}</div>
                                    <div className="form-group">
                                        <label htmlFor="userName"><h6>Username (*)</h6></label>
                                        <input type="text"
                                            className="form-control"
                                            name="userName"
                                            placeholder="Enter Username"
                                            value={this.state.userName}
                                            onChange={this.onChange} />
                                    </div>

                                    <div>{this.state.errorEmail == 'errorEmail' ? <NoEmail /> : null}</div>
                                    <div className="form-group">
                                        <label htmlFor="email"><h6>Email (*)</h6></label>
                                        <input type="text"
                                            className="form-control"
                                            name="email"
                                            placeholder="Enter Email"
                                            value={this.state.email}
                                            onChange={this.onChange} />
                                    </div>

                                    <div>{this.state.errorPassword == 'errorPassword' ? <NoPassword /> : null}</div>
                                    <div className="form-group">
                                        <label htmlFor="password"><h6>Parola (*)</h6></label>
                                        <input type="password"
                                            className="form-control"
                                            name="password"
                                            placeholder="Enter Password"
                                            value={this.state.password}
                                            onChange={this.onChange} />
                                    </div>
                                    {this.state.errorMessages == "passwordNotCorectly" ? <NoMatchPassword /> : null}

                                    <div className="form-group">
                                        <label htmlFor="password"><h6>Confirma parola (*)</h6></label>
                                        <input type="password"
                                            className="form-control"
                                            name="confirmPassword"
                                            placeholder="Enter Password"
                                            value={this.state.confirmPassword}
                                            onChange={this.onChange} />
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="phone"><h6>Numar de telefon</h6></label>
                                        <input type="number"
                                            className="form-control"
                                            name="phone"
                                            placeholder="Enter Phone"
                                            value={this.state.phone}
                                            onChange={this.onChange} />
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="address"><h6>Adresa</h6></label>
                                        <input type="text"
                                            className="form-control"
                                            name="address"
                                            placeholder="Enter Address"
                                            value={this.state.address}
                                            onChange={this.onChange} />
                                    </div>

                                    <p></p>

                                    <div>
                                        <h6>Zi de nastere</h6>
                                        <DatePicker selected={this.state.birthDate} onChange={this.onChangeBirthDate} />
                                    </div>

                                    <p></p>

                                    <label htmlFor="gender"><h6>Sex</h6></label>
                                    &nbsp;&nbsp;&nbsp;
                                    <Select
                                        labelId="demo-controlled-open-select-label"
                                        id="demo-controlled-open-select"
                                        label="Birthdate"
                                        open={this.state.open2}
                                        onClose={this.handleClose2}
                                        onOpen={this.handleOpen2}
                                        value={this.state.gender}
                                        onChange={this.handleChange2}
                                    >
                                        <MenuItem value={"Masculin"}>Masculin</MenuItem>
                                        <MenuItem value={"Feminin"}>Feminin</MenuItem>
                                    </Select>
                                    <p></p>

                                    <div className="form-group">
                                        <label htmlFor="salary"><h6>Salariu</h6></label>
                                        <input type="number"
                                            className="form-control"
                                            name="salary"
                                            placeholder="Enter Salary"
                                            value={this.state.salary}
                                            onChange={this.onChange} />
                                    </div>

                                    <label htmlFor="role"><h6>Rol</h6></label>
                                    &nbsp;&nbsp;&nbsp;
                                    <Select
                                        labelId="demo-controlled-open-select-label"
                                        id="demo-controlled-open-select"
                                        open={this.state.open}
                                        onClose={this.handleClose}
                                        onOpen={this.handleOpen}
                                        value={this.state.staffOcupationId}
                                        onChange={this.handleChange}
                                    >
                                        <MenuItem value={1}>Admin</MenuItem>
                                        <MenuItem value={2}>Doctor</MenuItem>
                                        <MenuItem value={3}>Asistent</MenuItem>
                                    </Select>
                                    <p></p>

                                    <Button id="button-style" type="submit" className="btn btn-secondary">Salveaza</Button>

                                </form>
                            </div>
                        </div>
                    </div>

                </Modal>

                <Modal2
                    className="white"
                    size="modal-body"
                    overlayClassName="Overlay"
                    aria-labelledby="example-modal-sizes-title-sm" isOpen={this.state.isActive2} onRequestClose={this.toggleModal2}>
                    <Button id="button-style" type="submit" onClick={this.closeAndSaveEdit} className="btn btn-secondary">Salveaza si inchide</Button>


                    <div>
                        <div className="col-sm-4 mx-auto">
                            <h1 className="text-center">Editare Personal</h1>
                        </div>


                        <table id="students">
                            <thead>

                            </thead>
                            <tbody>
                                <tr>
                                    <th>Nume</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>

                                            <div className="col">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.firstName} onChange={this.handleChangeFirstName} value={this.state.firstName} />
                                            </div>

                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Prenume</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div className="col-xs-4">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.lastName} onChange={this.handleChangeLastName} value={this.state.lastName} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Username</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div className="col-xs-4">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.userName} onChange={this.handleChangeUsername} value={this.state.userName} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>


                                <tr>
                                    <th>Email</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div className="col-xs-4">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.email} onChange={this.handleChangeEmail} value={this.state.email} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        Parola
                                    </th>
                                    <td><form onSubmit={this.handleSubmitEdit}>
                                        {this.state.errorMessages == "passwordNotCorectly" ? <NoMatchPassword /> : null}
                                        <div className="col-xs-4">
                                            <input type="password" className="form-control input-sm" placeholder={this.state.password} onChange={this.handleChangePassword} value={this.state.password} />
                                        </div>
                                        
                                        <Button id="button-style" onClick={this.handleOpenModalConfirmPassword} className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                    </form></td>
                                </tr>

                                <tr>
                                    <th>Numar de telefon</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div class="col-xs-4">
                                                <input type="text" className="form-control" id="ex3" placeholder={this.state.phone} onChange={this.handleChangePhone} value={this.state.phone} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>

                                        </form>
                                    </td>
                                </tr>


                                <tr>
                                    <th>Adresa</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" id="ex3" placeholder={this.state.address} onChange={this.handleChangeAddress} value={this.state.address} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Rol</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <InputLabel id="demo-controlled-open-select-label">Role</InputLabel>
                                            <Select

                                                className="form-control"
                                                open={this.state.open}
                                                onClose={this.handleClose}
                                                onOpen={this.handleOpen}
                                                value={this.state.staffOcupationId}
                                                onChange={this.handleChangeUserOcupation}
                                            >
                                                <MenuItem value={1}>Admin</MenuItem>
                                                <MenuItem value={2}>Doctor</MenuItem>
                                                <MenuItem value={3}>Asistent</MenuItem>
                                            </Select>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>


                                <tr>
                                    <th>Zi de nastere</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>

                                            <div>
                                                <DatePicker {...this.state.birthDate} placeholderText={this.state.birthDate} selected={this.state.birthDate} onChange={this.onChangeBirthDate} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>


                                    </td>
                                </tr>

                                <tr>
                                    <th>Salariu</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div className="col-xs-4">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.salary} onChange={this.handleChangeSalary} value={this.state.salary} />
                                            </div>
                                            <Button id="button-style" id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Sex</th>
                                    <td>{this.state.gender}</td>
                                </tr>

                            </tbody>

                        </table>

                        <p></p>
                        <p></p>
                        <p></p>

                    </div>

                </Modal2>

                <ModalConfirmDelete
                    show={this.state.showModalConfirmDelete}
                    onHide={this.handleCloseModalConfirmDelete}
                    removeStaff={this.deleteStaff}
                >
                </ModalConfirmDelete>

                <ModalConfirmPassword
                    show={this.state.showModalConfirmPassword}
                    onHide={this.handleCloseModalConfirmPassword}
                    saveEditPassword={this.handleSubmitEdit}
                    confirmPass={this.handleChangeConfirmPassword}
                >

                </ModalConfirmPassword>

                <div>


                </div>
                <Button onClick={this.toggleModalCreateStaff} className="btn btn-sm btn-primary float-right" id="fixedbutton">Creaza un cont</Button>
            </div>
        )

    }
}

export default Staff;