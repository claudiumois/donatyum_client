import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import AdminCard from './AdminCard.jsx';

class Doctors extends Component {
    constructor() {
        super();
        this.state = {
            admin: [],
        }
    }

    componentDidMount() {/*GET ALL Admins */
        const apiUrl = 'http://localhost:8080/api/staff/admins/find';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        admin: result
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )

    }

    render() {
        let adminCards = this.state.admin.map(admin => {
            return (
                <Col sm="4">
                    <AdminCard key={admin.idStaff} admin={admin} />
                </Col>
            )
        })
        return (
            <Container fluid className="back-color-staff">
                <Row>
                    {adminCards}
                </Row>
                
            </Container>
        )
    }
}

export default Doctors;