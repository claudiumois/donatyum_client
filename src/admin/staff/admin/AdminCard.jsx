import React, { Component } from 'react';
import {
    Card, CardImg, CardText, CardBlock,
    CardTitle, CardSubtitle, Button,CardBody
} from 'reactstrap';

import img from './doctor.jpg'

class DoctorCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let { firstName, lastName, email, phone, birthDate } = this.props.admin;
        return (
            <div>
                <Card>
                    <CardImg top width="100%" src={img} alt="Card image cap" />
                    <CardBody>
                        <CardTitle><h6>Nume: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{firstName} {lastName}</h6></CardTitle>
                        <CardTitle><h6>Email: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{email}</h6></CardTitle>
                        <CardTitle><h6>Telefon: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0{phone}</h6></CardTitle>
                        <CardTitle><h6>Nascut in: &nbsp;&nbsp;&nbsp;{birthDate}</h6></CardTitle>
                    </CardBody>
                </Card>
            </div>
        )
    }
}
export default DoctorCard;