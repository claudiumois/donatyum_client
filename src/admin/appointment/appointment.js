import React from 'react';

class Appointment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
        }

    }

    componentDidMount() {/*GET ALL Appointments */
        const apiUrl = 'http://localhost:8080/api/appointment/getAllAppointments';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {


                    this.setState({
                        appointments: result,
                    });
                    console.log(result);
                },
                (error) => {
                    this.setState({ error });
                }
            )
    }

    render() {


        const { appointments } = this.state;

        return (

            <div className="back-color">
                <hr />
                <h1 id='title'>Programari</h1>

                <table id='students'>
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Ora</th>
                            <th>Donator</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map(appointment => (
                            <tr key={appointment.idAppointment}>


                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.firstName} {appointment.lastName}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )

    }
}

export default Appointment;