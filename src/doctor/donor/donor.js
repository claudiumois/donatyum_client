import React from 'react';


class Donor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            donors: [], 
        }
     
    }

    componentDidMount() {/*GET ALL Donors */
        const apiUrl = 'http://localhost:8080/api/customer/find';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        donors: result
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )

    }

    render() {


        const { donors } = this.state;

        return (

            <div className="back-color">
                <hr />
                <h1 id='title'>Donatori</h1>

                <table id='students'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>BirthDate</th>
                            <th>Gender</th>
                            <th>Address</th>
                            <th>Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        {donors.map(donor => (
                            <tr key={donor.idCustomer}>

                                <td>{donor.firstName} {donor.lastName}</td>
                                <td>{donor.userName}</td>
                                <td>{donor.email}</td>
                                <td>{donor.password}</td>
                                <td>{donor.birthDate}</td>
                                <td>{donor.gender}</td>
                                <td>{donor.address}</td>
                                <td>0{donor.phone}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                </div>
        )

    }
}

export default Donor;