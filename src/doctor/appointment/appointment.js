import React from 'react';
import ModalCheckQ from 'react-modal';
import { Button } from 'react-bootstrap';
import Moment from 'moment';

import '../style.css'

class Appointment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            isActiveForCheckQ: false,
            idCustomer: 0,
            date: '',
            time: '',
            qs: [],
            nameDonor: '',
            appointmentId: 0,
        }
    }

    toggleModalForChackQ = () => {
        this.setState({
            isActiveForCheckQ: !this.state.isActiveForCheckQ
        })
    }

    componentWillMount() {
        ModalCheckQ.setAppElement('body');
    }

    rememberIdDonorForCheckQ = (id, idAppointment, date, time) => {

        this.setState({
            appointmentId: idAppointment,
            idCustomer: id,
            date: date,
            time: time,
        })

        const apiUrlForGetAllQFromCustomer = 'http://localhost:8080/api/q/getAllQ/byCustomer/' + id;

        fetch(apiUrlForGetAllQFromCustomer)
            .then(res => res.json())
            .then(
                (result) => {


                    this.setState({
                        qs: result,
                    });
                    console.log(this.state.qs);
                },
                (error) => {
                    this.setState({ error });
                }
            )

        const apiUrlForDetailsDonor = 'http://localhost:8080/api/appointment/getAppointmentByCustomerId/' + id;

        fetch(apiUrlForDetailsDonor)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        nameDonor: result.firstName + " " + result.lastName,
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )


        this.toggleModalForChackQ();
    }

    componentDidMount() {/*GET ALL Appointments */
        const apiUrl = 'http://localhost:8080/api/appointment/getAllAppointmentsWithStatusZero';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {


                    this.setState({
                        appointments: result,
                    });
                    console.log(result);
                },
                (error) => {
                    this.setState({ error });
                }
            )
    }

    deleteAppointment(id) {/*DELETE Appointment FROM CUSTOMER*/

        const apiUrl = 'http://localhost:8080/api/appointment/delete/' + id;
        const formData = new FormData();
        formData.append('id', id);

        const options = {
            method: 'DELETE',
            body: formData
        }

        fetch(apiUrl, options)
            .then(res => res.json())

        /**Trimite mesaj inapoi la client si spune ca nu poate sa doneze sange */

         const optionsForSenderMail = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        const apiForRejectAppointment = "http://localhost:8080/sendMailForRejectAppointment/ToCustomer/" + this.state.idCustomer;

        console.log(apiForRejectAppointment);

        fetch(apiForRejectAppointment, optionsForSenderMail)
            .then(res => res.json())
            .then(res => console.log(res))


        /******************************************************************/

        window.location.reload(false);
    }

    acceptAppointment(idCustomer) {/*STATUS = 1 */
        const apiUrl = 'http://localhost:8080/api/appointment/updateAppointment/' + idCustomer + '/accept';
        const formData = new FormData();
        formData.append('id', idCustomer);

        const options = {
            method: 'PUT',
            body: formData
        }

        fetch(apiUrl, options)
            .then(res => res.json())


        /**Trimite mesaj inapoi la client ca a fost acceptata programarea */


        const optionsForSenderMail = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        var date = Moment(this.state.date).format('YYYY-MM-DD')
        const apiForConfirmAppointment = "http://localhost:8080/sendMailToCustomer/" + idCustomer + "/" + date + "/" + this.state.time;

        console.log(apiForConfirmAppointment);

        fetch(apiForConfirmAppointment, optionsForSenderMail)
            .then(res => res.json())
            .then(res => console.log(res))


        /******************************************************************/
        window.location.reload(false);

    }

    converAnswer = (answer) => {

        if (answer == 'a') {
            return 'DA';
        } else if (answer == 'b') {
            return 'NU';
        }
    }


    render() {


        const { appointments } = this.state;

        return (

            <div className="back-color">
                <hr />
                <h1 id='title'>Programari</h1>

                <table id='students'>
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Ora</th>
                            <th>Donator</th>
                            <th>Verificare</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => (
                            <tr key={appointment.idAppointment}>


                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.firstName} {appointment.lastName}</td>
                                <td>
                                    <button type="button" onClick={() => { this.rememberIdDonorForCheckQ(appointment.idCustomer, appointment.id, appointment.date, appointment.time) }} class="btn btn-info">Verifica</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>

                <ModalCheckQ
                    className="white"
                    size="modal-body"
                    overlayClassName="Overlay"
                    aria-labelledby="example-modal-sizes-title-sm" isOpen={this.state.isActiveForCheckQ} onRequestClose={this.toggleModalForChackQ}>
                    <Button id="button-style" type="submit" onClick={this.toggleModalForChackQ} className="btn btn-secondary">Inchide</Button>


                    <div>
                        <div className="col-sm-4 mx-auto">
                            <h1 className="text-center">Donator {this.state.nameDonor}</h1>
                        </div>

                        <table id='students'>
                            <thead>
                                <tr>
                                    <th>Intrebare</th>
                                    <th>Respunsul dat</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.qs.map(q => (
                                    <tr key={q.id}>
                                        <td>{q.question} ?</td>
                                        <td>{this.converAnswer(q.answerCustomer)}</td>
                                    </tr>
                                ))}
                            </tbody>

                            <button type="button" onClick={() => { this.acceptAppointment(this.state.idCustomer) }} class="btn btn-success">Accepta</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" onClick={() => { this.deleteAppointment(this.state.appointmentId) }} class="btn btn-danger">Respinge</button>
                        </table>



                    </div>


                </ModalCheckQ>

            </div>


        )

    }
}

export default Appointment;