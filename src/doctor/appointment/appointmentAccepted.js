import React from 'react';
import ModalCheckAnaliza from 'react-modal';
import ModalCheckAnalizaFinala from 'react-modal';
import { Button } from 'react-bootstrap';

import '../../admin/staff/style.css'

class Appointment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            isActiveForCheckAnaliza: false,
            isActiveForCheckAnalizaFinala: false,
            idCustomer: 0,
            date: '',
            time: '',
            analize: [],
            analizeFinale: [],
            nameDonor: '',
            appointmentId: 0,
        }
    }

    toggleModalForCheckAnaliza = () => {
        this.setState({
            isActiveForCheckAnaliza: !this.state.isActiveForCheckAnaliza,
            analize: [],
        })
    }

    toggleModalForCheckAnalizaFinala = () => {
        this.setState({
            isActiveForCheckAnalizaFinala: !this.state.isActiveForCheckAnalizaFinala,
            analizeFinale: [],
        })
    }



    componentWillMount() {
        ModalCheckAnaliza.setAppElement('body');
    }

    componentWillMount() {
        ModalCheckAnalizaFinala.setAppElement('body');
    }

    rememberIdDonorForCheckAnalize = (id, idAppointment) => {

        this.setState({
            appointmentId: idAppointment,
            idCustomer: id,
        })

        console.log('customer id = ' + id);

        const apiUrlForGetAnaliza = 'http://localhost:8080/api/testAbleDonate/findTestAnaliza/' + id;
        console.log('api=' + apiUrlForGetAnaliza)

        fetch(apiUrlForGetAnaliza)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        analize: result,
                    });
                    console.log('analiza = ' + this.state.analize);
                },
                (error) => {
                    this.setState({ error });
                }
            )

        console.log("Analize: " + this.state.analize);


        const apiUrlForDetailsDonor = 'http://localhost:8080/api/appointment/getAppointmentByCustomerId/' + id;

        fetch(apiUrlForDetailsDonor)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        nameDonor: result.firstName + " " + result.lastName,
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )


        this.toggleModalForCheckAnaliza();
    }

    rememberIdDonorForCheckAnalizeFinal = (id, idAppointment) => {

        this.setState({
            appointmentId: idAppointment,
            idCustomer: id,
        })

        console.log('customer id = ' + id);

        const apiUrlForGetAnalizaFinala = 'http://localhost:8080/api/testBlood/findTestAnaliza/' + id;
        console.log('api=' + apiUrlForGetAnalizaFinala)

        fetch(apiUrlForGetAnalizaFinala)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        analizeFinale: result,
                    });
                    console.log('analiza Finala = ' + result);
                },
                (error) => {
                    this.setState({ error });
                }
            )

        console.log("Analize: " + this.state.analizeFinale);


        const apiUrlForDetailsDonor = 'http://localhost:8080/api/appointment/getAppointmentByCustomerId/' + id;

        fetch(apiUrlForDetailsDonor)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        nameDonor: result.firstName + " " + result.lastName,
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )


        this.toggleModalForCheckAnalizaFinala();
    }

    componentDidMount() {/*GET ALL Appointments */
        const apiUrl = 'http://localhost:8080/api/appointment/getAllAppointmentsWithStatusUnu';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {


                    this.setState({
                        appointments: result,
                    });
                    console.log(result);
                },
                (error) => {
                    this.setState({ error });
                }
            )
    }

    showAnalize = () => {
        const { analize } = this.state;

        if (analize.id != '' && analize.id != null) {
            return (
                <div>
                    <div className="col-sm-4 mx-auto">
                        <h1 className="text-center">Analiza pre-donare a donatorului {this.state.nameDonor}</h1>
                    </div>
                    <div>
                        <table id='students'>
                            <thead>
                                <tr>
                                    <th>Tip sange</th>
                                    <th>Temperatura corpului</th>
                                    <th>Tensiune</th>
                                    <th>Glicemie</th>
                                    <th>Hemoglobina</th>
                                    <th>Inaltime</th>
                                    <th>Greutate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr key={analize.id}>
                                    <td>{analize.typeBlood} </td>
                                    <td>{analize.bodyTemperature} </td>
                                    <td>{analize.tension} </td>
                                    <td>{analize.glicemie} </td>
                                    <td>{analize.hemoglobina} </td>
                                    <td>{analize.height} </td>
                                    <td>{analize.weight} kg</td>
                                </tr>

                            </tbody>

                        </table>
                    </div>


                </div>
            )
        }
        else {
            return (
                <div><h4>Nu s-a facut inca Pre-Donarea.</h4></div>
            )
        }
    }

    showValuePozitivOrNegativForFinalAnalize = (e) => {
        if (e == true) {
            return (<div class="alert alert-danger" role="alert">Pozitiv</div>)
        } else if (e == false) {
            return (<div class="alert alert-success" role="alert">Negativ</div>)
        }
    }

    showAnalizeFinale = () => {
        const { analizeFinale } = this.state;

        if (analizeFinale.id != '' && analizeFinale.id != null) {
            return (
                <div>
                    <div className="col-sm-4 mx-auto">
                        <h1 className="text-center">Analiza finala a donatorului {this.state.nameDonor}</h1>
                    </div>
                    <div>
                        <table id='students'>
                            <thead>
                                <tr>
                                    <th>HIV</th>
                                    <th>HTLV</th>
                                    <th>HBS</th>
                                    <th>HCV</th>
                                    <th>ALT</th>
                                    <th>GOT</th>
                                    <th>GPT</th>
                                    <th>Optiune</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr key={analizeFinale.id}>
                                    <td>{this.showValuePozitivOrNegativForFinalAnalize(analizeFinale.hiv)} </td>
                                    <td>{this.showValuePozitivOrNegativForFinalAnalize(analizeFinale.htlv)} </td>
                                    <td>{this.showValuePozitivOrNegativForFinalAnalize(analizeFinale.hbs)} </td>
                                    <td>{this.showValuePozitivOrNegativForFinalAnalize(analizeFinale.hcv)} </td>
                                    <td>{this.showValuePozitivOrNegativForFinalAnalize(analizeFinale.alt)} </td>
                                    <td>{this.showValuePozitivOrNegativForFinalAnalize(analizeFinale.got)} </td>
                                    <td>{this.showValuePozitivOrNegativForFinalAnalize(analizeFinale.gpt)} </td>
                                    <td>
                                        <Button id="button-style" variant="danger" onClick={() => { this.novalid(this.state.idCustomer) }} class="btn btn-danger">Sange invalid</Button>
                                        &nbsp;&nbsp;&nbsp;
                                        <Button id="button-style" variant="success" onClick={() => { this.valid(this.state.idCustomer) }} class="btn btn-success">Sange valid</Button>
                                    </td>
                                </tr>

                            </tbody>


                        </table>
                    </div>
                </div>
            )
        }
        else {
            return (
                <div><h4>Nu s-a facut afisat rezultatele.</h4></div>
            )
        }
    }

    valid = (idCustomer) => {
        /*Creare analiza finala pentru donator la compartimentul de donare si o pune pe status 1 pentru ca sangele este valid 
          pentru a fi dat mai departe la pacientii bolnavi
        */

        const analizaFinala = {
            idCustomer: this.state.idCustomer,
            status: 1,
        }

        const options = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        var apiUrl = 'http://localhost:8080/api/blood/setBloodFromCustomer/' + analizaFinala.idCustomer + "/" + analizaFinala.status;

        console.log(apiUrl);

        fetch(apiUrl, options)
            .then(res => res.json())
            .then(res => console.log(res))

        /***********************************************************************************************************/

        /**Trimite mesaj inapoi la client cu Rezultatele analizelor */

        const analiza = {
            hiv: this.state.analizeFinale.hiv,
            htlv: this.state.analizeFinale.htlv,
            got: this.state.analizeFinale.got,
            gpt: this.state.analizeFinale.gpt,
            hbs: this.state.analizeFinale.hbs,
            hcv: this.state.analizeFinale.hcv,
            alt: this.state.analizeFinale.alt,
        }


        const optionsForSenderMail = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        const apiForSendMailWithFinalAnalizeToCustomer = "http://localhost:8080/sendMailToCustomerWithFinalAnalize/" + idCustomer + "/" +
            analiza.hiv + "/" + analiza.hbs + "/" + analiza.hcv + "/" + analiza.htlv + "/" + analiza.alt + "/" + analiza.got + "/"
            + analiza.gpt;

        console.log(apiForSendMailWithFinalAnalizeToCustomer);

        fetch(apiForSendMailWithFinalAnalizeToCustomer, optionsForSenderMail)
            .then(res => res.json())
            .then(res => console.log(res))


        /******************************************************************/
    }

    novalid = (idCustomer) => {
        /*Creare analiza finala pentru donator la compartimentul de donare si o pune pe status 0 pentru ca sangele nu este valid 
          pentru a fi dat mai departe la pacientii bolnavi
        */

        const analizaFinala = {
            idCustomer: this.state.idCustomer,
            status: 0,
        }

        const options = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        var apiUrl = 'http://localhost:8080/api/blood/setBloodFromCustomer/' + analizaFinala.idCustomer + "/" + analizaFinala.status;

        console.log(apiUrl);

        fetch(apiUrl, options)
            .then(res => res.json())
            .then(res => console.log(res))

        /***********************************************************************************************************/

        /**Trimite mesaj inapoi la client cu Rezultatele analizelor */

        const analiza = {
            hiv: this.state.analizeFinale.hiv,
            htlv: this.state.analizeFinale.htlv,
            got: this.state.analizeFinale.got,
            gpt: this.state.analizeFinale.gpt,
            hbs: this.state.analizeFinale.hbs,
            hcv: this.state.analizeFinale.hcv,
            alt: this.state.analizeFinale.alt,
        }


        const optionsForSenderMail = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        const apiForSendMailWithFinalAnalizeToCustomer = "http://localhost:8080/sendMailToCustomerWithFinalAnalize/" + idCustomer + "/" +
            analiza.hiv + "/" + analiza.hbs + "/" + analiza.hcv + "/" + analiza.htlv + "/" + analiza.alt + "/" + analiza.got + "/"
            + analiza.gpt;

        console.log(apiForSendMailWithFinalAnalizeToCustomer);

        fetch(apiForSendMailWithFinalAnalizeToCustomer, optionsForSenderMail)
            .then(res => res.json())
            .then(res => console.log(res))


        /******************************************************************/
    }

    render() {


        const { analize } = this.state;

        return (

            <div className="back-color">
                <hr />
                <h1 id='title'>Programari Acceptate</h1>

                <table id='students'>
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Ora</th>
                            <th>Donator</th>
                            <th>Pre-donare</th>
                            <th>Compartiment donare</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => (
                            <tr key={appointment.idAppointment}>


                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.firstName} {appointment.lastName}</td>
                                <td>
                                    <button type="button" onClick={() => { this.rememberIdDonorForCheckAnalize(appointment.idCustomer, appointment.id) }} class="btn btn-info">Verifica Analiza</button>
                                </td>
                                <td>
                                    <button type="button" onClick={() => { this.rememberIdDonorForCheckAnalizeFinal(appointment.idCustomer, appointment.id) }} class="btn btn-info">Verifica Analiza</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>

                <ModalCheckAnaliza
                    className="white"
                    size="modal-body"
                    overlayClassName="Overlay"
                    aria-labelledby="example-modal-sizes-title-sm" isOpen={this.state.isActiveForCheckAnaliza} onRequestClose={this.toggleModalForCheckAnaliza}>
                    <Button id="button-style" type="submit" onClick={this.toggleModalForCheckAnaliza} className="btn btn-secondary">Inchide</Button>


                    {this.showAnalize()}


                </ModalCheckAnaliza>

                <ModalCheckAnalizaFinala
                    className="white"
                    size="modal-body"
                    overlayClassName="Overlay"
                    aria-labelledby="example-modal-sizes-title-sm" isOpen={this.state.isActiveForCheckAnalizaFinala} onRequestClose={this.toggleModalForCheckAnalizaFinala}>
                    <Button id="button-style" type="submit" onClick={this.toggleModalForCheckAnalizaFinala} className="btn btn-secondary">Inchide</Button>


                    {this.showAnalizeFinale()}


                </ModalCheckAnalizaFinala>

            </div>


        )

    }
}

export default Appointment;