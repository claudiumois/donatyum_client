import React from 'react';
import ModalSetAnaliza from 'react-modal';
import ModalSetAnalizaFinala from 'react-modal';
import { Button } from 'reactstrap';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

class AppointmentAssistent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            isActiveForAnaliza: false,
            isActiveForAnalizaFinala: false,
            open: false,

            openhiv: false,
            hiv: false,
            openhtlv: false,
            htlv: false,
            openhbs: false,
            hbs: false,
            openhcv: false,
            hcv: false,
            openalt: false,
            alt: false,
            opengot: false,
            got: false,
            opengpt: false,
            gpt: false,

            idCustomer: '',
            name: '',
            typeblood: '',
            bodyTemperature: '',
            glicemie: '',
            height: '',
            weight: '',
            hemoglobina: '',
            tension: '',
            nameDonor: '',
            appointmentId: 0,
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onSubmitFinalAnaliza = this.onSubmitFinalAnaliza.bind(this);
    }


    handleClose = () => {
        this.setState({ open: false })
    };

    handleOpen = () => {
        this.setState({ open: true })
    };

    handleCloseHiv = () => {
        this.setState({ openhiv: false })
    };

    handleOpenHiv = () => {
        this.setState({ openhiv: true })
    };

    handleCloseHtlv = () => {
        this.setState({ openhtlv: false })
    };

    handleOpenHtlv = () => {
        this.setState({ openhtlv: true })
    };

    handleCloseHbs = () => {
        this.setState({ openhbs: false })
    };

    handleOpenHbs = () => {
        this.setState({ openhbs: true })
    };
    handleCloseHcv = () => {
        this.setState({ openhcv: false })
    };

    handleOpenHcv = () => {
        this.setState({ openhcv: true })
    };

    handleCloseAlt = () => {
        this.setState({ openalt: false })
    };

    handleOpenAlt = () => {
        this.setState({ openalt: true })
    };

    handleCloseGot = () => {
        this.setState({ opengot: false })
    };

    handleOpenGot = () => {
        this.setState({ opengot: true })
    };

    handleCloseGpt = () => {
        this.setState({ opengpt: false })
    };

    handleOpenGpt = () => {
        this.setState({ opengpt: true })
    };

    toggleModalForAnaliza = () => {
        this.setState({
            isActiveForAnaliza: !this.state.isActiveForAnaliza
        })
    }

    toggleModalForAnalizaFinala = () => {
        this.setState({
            isActiveForAnalizaFinala: !this.state.isActiveForAnalizaFinala
        })
    }

    componentWillMount() {
        ModalSetAnaliza.setAppElement('body');
    }

    componentWillMount() {
        ModalSetAnalizaFinala.setAppElement('body');
    }

    rememberIdDonorForCheckQ = (id, idAppointment, firstName, lastName) => {

        this.setState({
            appointmentId: idAppointment,
            idCustomer: id,
            name: firstName + " " + lastName,
        })

        console.log("id= " + id + " idAppointment: " + idAppointment, "name=" + firstName)

        const apiUrlForDetailsDonor = 'http://localhost:8080/api/appointment/getAppointmentByCustomerId/' + id;

        fetch(apiUrlForDetailsDonor)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        nameDonor: result.firstName + " " + result.lastName,
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )


        this.toggleModalForAnaliza();
    }

    rememberIdDonorForSetLastAnalize = (id, idAppointment, firstName, lastName) => {

        this.setState({
            appointmentId: idAppointment,
            idCustomer: id,
            name: firstName + " " + lastName,
        })

        console.log("id= " + id + " idAppointment: " + idAppointment, "name=" + firstName)

        const apiUrlForDetailsDonor = 'http://localhost:8080/api/appointment/getAppointmentByCustomerId/' + id;

        fetch(apiUrlForDetailsDonor)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        nameDonor: result.firstName + " " + result.lastName,
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )


        this.toggleModalForAnalizaFinala();
    }

    componentDidMount() {/*GET ALL Appointments */
        const apiUrl = 'http://localhost:8080/api/appointment/findAllAppointmentsChecked';

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {

                    this.setState({
                        appointments: result,
                    });
                    console.log(result);
                },
                (error) => {
                    this.setState({ error });
                }
            )
    }

    onSubmit(e) {
        e.preventDefault()


        /*Creare analiza pentru donator la pre-donare */

        const test = {
            idCustomer: this.state.idCustomer,
            typeblood: this.state.typeblood,
            name: this.state.name,
            bodyTemperature: this.state.bodyTemperature,
            glicemie: this.state.glicemie,
            height: this.state.height,
            weight: this.state.weight,
            hemoglobina: this.state.hemoglobina,
            tension: this.state.tension,
        }

        const options = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        var apiUrl = 'http://localhost:8080/api/testAbleDonate/setAnalizaToCustomer/' +
            test.idCustomer + '/' + test.typeblood + '/' + test.bodyTemperature + '/' + test.glicemie + '/' + test.height + '/' + test.weight + '/' + test.hemoglobina + '/' + test.tension;

        console.log(apiUrl);

        fetch(apiUrl, options)
            .then(res => res.json())
            .then(res => console.log(res))

        /***************************************/

        /************Notifica toti doctori cu privire la programarea respectiva****************/
        var apiForSendMailToDoctors = "http://localhost:8080/sendMailToDoctorsWithAnaliza/" + test.name + '/' +
            test.typeblood + '/' + test.bodyTemperature + '/' + test.glicemie + '/' + test.height + '/'
            + test.weight + '/' + test.hemoglobina + '/' + test.tension;

        console.log(apiForSendMailToDoctors);

        const optionsForSenderMailToDoctorsWithAnalize = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }

        fetch(apiForSendMailToDoctors, optionsForSenderMailToDoctorsWithAnalize)
            .then(res => res.json())
            .then(res => console.log(res))

        /******************************************************************************************/

        window.location.reload(false);
    }

    onSubmitFinalAnaliza(e) {
        e.preventDefault()


        /*Creare analiza pentru donator la pre-donare */

        const analizaFinala = {
            idCustomer: this.state.idCustomer,
            hiv: this.state.hiv,
            htlv: this.state.htlv,
            hbs: this.state.hbs,
            hcv: this.state.hcv,
            alt: this.state.alt,
            got: this.state.got,
            gpt: this.state.gpt,
        }

        const options = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        var apiUrl = 'http://localhost:8080/api/testBlood/setAnalizaFinalaToCustomer/' +
            analizaFinala.idCustomer + "/" + analizaFinala.hiv + "/" + analizaFinala.htlv + "/"
            + analizaFinala.hbs + "/" + analizaFinala.hcv + "/" + analizaFinala.alt + "/"
            + analizaFinala.got + "/" + analizaFinala.gpt;

        console.log(apiUrl);

        fetch(apiUrl, options)
            .then(res => res.json())
            .then(res => console.log(res))

        /***************************************/

        /************Notifica toti doctori cu privire la programarea respectiva****************/
        var apiForSendMailToDoctors = "http://localhost:8080/sendMailToDoctorsForFinalAnalize/" + analizaFinala.idCustomer;

        console.log(apiForSendMailToDoctors);

        const optionsForSenderMailToDoctorsWithAnalize = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }

        fetch(apiForSendMailToDoctors, optionsForSenderMailToDoctorsWithAnalize)
            .then(res => res.json())
            .then(res => console.log(res))

        /******************************************************************************************/

        window.location.reload(false);
    }

    handleChangeTypeBlood = (event) => {
        this.setState({ typeblood: event.target.value })
    };

    handleChangeHiv = (event) => {
        this.setState({ hiv: event.target.value })
    };

    handleChangeHtlv = (event) => {
        this.setState({ htlv: event.target.value })
    };

    handleChangeHbs = (event) => {
        this.setState({ hbs: event.target.value })
    };

    handleChangeHcv = (event) => {
        this.setState({ hcv: event.target.value })
    };

    handleChangeAlt = (event) => {
        this.setState({ alt: event.target.value })
    };

    handleChangeGot = (event) => {
        this.setState({ got: event.target.value })
    };

    handleChangeGpt = (event) => {
        this.setState({ gpt: event.target.value })
    };

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    render() {

        return (

            <div className="back-color">
                <hr />
                <h1 id='title'>Programari</h1>

                <table id='students'>
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Ora</th>
                            <th>Donator</th>
                            <th>Pre-donare</th>
                            <th>Analize finale</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointments.map(appointment => (
                            <tr key={appointment.idAppointment}>


                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.firstName} {appointment.lastName}</td>
                                <td>
                                    <button type="button" onClick={() => { this.rememberIdDonorForCheckQ(appointment.idCustomer, appointment.id, appointment.firstName, appointment.lastName) }} class="btn btn-info">Asignare</button>
                                </td>
                                <td>
                                    <button type="button" onClick={() => { this.rememberIdDonorForSetLastAnalize(appointment.idCustomer, appointment.id, appointment.firstName, appointment.lastName) }} class="btn btn-info">Asignare</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>

                <ModalSetAnaliza
                    className="white"
                    size="modal-body"
                    overlayClassName="Overlay"
                    aria-labelledby="example-modal-sizes-title-sm" isOpen={this.state.isActiveForAnaliza} onRequestClose={this.toggleModalForAnaliza}>




                    <Button id="button-style" type="submit" onClick={this.toggleModalForAnaliza} className="btn btn-secondary">Inchide</Button>

                    <div >

                        <div className="row"></div>
                        <div className="col-md-6 mt-5 mx-auto">
                            <form noValidate onSubmit={this.onSubmit}>

                                <label htmlFor="typeblood"><h6>Tip sange</h6></label>
                                        &nbsp;&nbsp;&nbsp;
                                        <Select
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select"
                                    label="typeblood"
                                    open={this.state.open}
                                    onClose={this.handleClose}
                                    onOpen={this.handleOpen}
                                    value={this.state.typeblood}
                                    onChange={this.handleChangeTypeBlood}
                                >
                                    <MenuItem value={"0rh-"}>0 rh-</MenuItem>
                                    <MenuItem value={"0 rh+"}>0 rh+</MenuItem>
                                    <MenuItem value={"A rh-"}>A rh-</MenuItem>
                                    <MenuItem value={"A rh+"}>A rh+</MenuItem>
                                    <MenuItem value={"B rh-"}>B rh-</MenuItem>
                                    <MenuItem value={"B rh+"}>B rh+</MenuItem>
                                    <MenuItem value={"AB rh-"}>AB rh-</MenuItem>
                                    <MenuItem value={"AB rh+"}>AB rh+</MenuItem>
                                </Select>

                                <div className="form-group">
                                    <label htmlFor="bodyTemperature">Temperatura corporala</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="bodyTemperature"
                                        placeholder="Introduceti temperatura corpului"
                                        value={this.state.bodyTemperature}
                                        onChange={this.onChange}
                                    />
                                </div>



                                <div className="form-group">
                                    <label htmlFor="glicemie">Glicemie</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        name="glicemie"
                                        placeholder="Introduceti glicemia"
                                        value={this.state.glicemie}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="height">Inaltime</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        name="height"
                                        placeholder="Introduceti inaltimea"
                                        value={this.state.height}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="weight">Greutate</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        name="weight"
                                        placeholder="Introduceti greutatea"
                                        value={this.state.weight}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="hemoglobina">Hemoglobina</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        name="hemoglobina"
                                        placeholder="Introduceti hemoglobina"
                                        value={this.state.hemoglobina}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="tension">Tensiunea</label>
                                    <input
                                        type="number"
                                        className="form-control"
                                        name="tension"
                                        placeholder="Introduceti tensiunea"
                                        value={this.state.tension}
                                        onChange={this.onChange}
                                    />
                                </div>




                                <p></p>
                                <div>
                                    <button type="submit" class="btn btn-primary">Trimite analiza</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </ModalSetAnaliza>


                <ModalSetAnalizaFinala
                    className="white"
                    size="modal-body"
                    overlayClassName="Overlay"
                    aria-labelledby="example-modal-sizes-title-sm" isOpen={this.state.isActiveForAnalizaFinala} onRequestClose={this.toggleModalForAnalizaFinala}>




                    <Button id="button-style" type="submit" onClick={this.toggleModalForAnalizaFinala} className="btn btn-secondary">Inchide</Button>

                    <div >

                        <div className="row"></div>
                        <div className="col-md-6 mt-5 mx-auto">
                            <form noValidate onSubmit={this.onSubmitFinalAnaliza}>

                                <label htmlFor="typeblood"><h6>Hiv</h6></label>
                                        &nbsp;&nbsp;&nbsp;
                                        <Select
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select"
                                    label="typeblood"
                                    open={this.state.openhiv}
                                    onClose={this.handleCloseHiv}
                                    onOpen={this.handleOpenHiv}
                                    value={this.state.hiv}
                                    onChange={this.handleCloseHiv}
                                >
                                    <MenuItem value={false}>Negativ</MenuItem>
                                    <MenuItem value={true}>Pozitiv</MenuItem>
                                </Select>
                                <br />
                                <br />

                                <label htmlFor="typeblood"><h6>HTLV</h6></label>
                                        &nbsp;&nbsp;&nbsp;
                                        <Select
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select"
                                    label="typeblood"
                                    open={this.state.openhtlv}
                                    onClose={this.handleCloseHtlv}
                                    onOpen={this.handleOpenHtlv}
                                    value={this.state.htlv}
                                    onChange={this.handleChangeHtlv}
                                >
                                    <MenuItem value={false}>Negativ</MenuItem>
                                    <MenuItem value={true}>Pozitiv</MenuItem>
                                </Select>
                                <br />
                                <br />

                                <label htmlFor="typeblood"><h6>HBS</h6></label>
                                        &nbsp;&nbsp;&nbsp;
                                        <Select
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select"
                                    label="typeblood"
                                    open={this.state.openhbs}
                                    onClose={this.handleCloseHbs}
                                    onOpen={this.handleOpenHbs}
                                    value={this.state.hbs}
                                    onChange={this.handleChangeHbs}
                                >
                                    <MenuItem value={false}>Negativ</MenuItem>
                                    <MenuItem value={true}>Pozitiv</MenuItem>
                                </Select>
                                <br />
                                <br />
                                <label htmlFor="typeblood"><h6>HCV</h6></label>
                                        &nbsp;&nbsp;&nbsp;
                                        <Select
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select"
                                    label="typeblood"
                                    open={this.state.openhcv}
                                    onClose={this.handleCloseHcv}
                                    onOpen={this.handleOpenHcv}
                                    value={this.state.hcv}
                                    onChange={this.handleChangeHcv}
                                >
                                    <MenuItem value={false}>Negativ</MenuItem>
                                    <MenuItem value={true}>Pozitiv</MenuItem>
                                </Select>
                                <br />
                                <br />
                                <label htmlFor="typeblood"><h6>ALT</h6></label>
                                        &nbsp;&nbsp;&nbsp;
                                        <Select
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select"
                                    label="typeblood"
                                    open={this.state.openalt}
                                    onClose={this.handleCloseAlt}
                                    onOpen={this.handleOpenAlt}
                                    value={this.state.alt}
                                    onChange={this.handleChangeAlt}
                                >
                                    <MenuItem value={false}>Negativ</MenuItem>
                                    <MenuItem value={true}>Pozitiv</MenuItem>
                                </Select>
                                <br />
                                <br />
                                <label htmlFor="typeblood"><h6>GOT</h6></label>
                                        &nbsp;&nbsp;&nbsp;
                                        <Select
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select"
                                    label="typeblood"
                                    open={this.state.opengot}
                                    onClose={this.handleCloseGot}
                                    onOpen={this.handleOpenGot}
                                    value={this.state.got}
                                    onChange={this.handleChangeGot}
                                >
                                    <MenuItem value={false}>Negativ</MenuItem>
                                    <MenuItem value={true}>Pozitiv</MenuItem>
                                </Select>
                                <br />
                                <br />
                                <label htmlFor="typeblood"><h6>GPT</h6></label>
                                        &nbsp;&nbsp;&nbsp;
                                        <Select
                                    labelId="demo-controlled-open-select-label"
                                    id="demo-controlled-open-select"
                                    label="typeblood"
                                    open={this.state.opengpt}
                                    onClose={this.handleCloseGpt}
                                    onOpen={this.handleOpenGpt}
                                    value={this.state.gpt}
                                    onChange={this.handleChangeGpt}
                                >
                                    <MenuItem value={false}>Negativ</MenuItem>
                                    <MenuItem value={true}>Pozitiv</MenuItem>
                                </Select>




                                <p></p>
                                <div>
                                    <button type="submit" class="btn btn-primary">Trimite analiza</button>
                                </div>

                            </form>
                        </div>
                    </div>




                </ModalSetAnalizaFinala>

            </div>


        )

    }
}

export default AppointmentAssistent;