import React, {Component} from 'react';

export default class HasDeleted extends Component{
    render(){
        return(
            <div class="alert alert-success" role="alert">
                Stergerea a avut loc cu success.
            </div>
        )
    }

}