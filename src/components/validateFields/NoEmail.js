import React, {Component} from 'react';

export default class NoEmail extends Component{
    render(){
        return(
            <div class="alert alert-danger" role="alert">
                Spune-ne email-ul tau.
            </div>
        )
    }

}