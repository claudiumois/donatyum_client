import React, {Component} from 'react';

export default class NoName extends Component{
    render(){
        return(
            <div class="alert alert-danger" role="alert">
                Spune-ne cine esti.
            </div>
        )
    }

}