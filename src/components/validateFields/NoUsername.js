import React, {Component} from 'react';

export default class NoUsername extends Component{
    render(){
        return(
            <div class="alert alert-danger" role="alert">
                Alege un username.
            </div>
        )
    }

}