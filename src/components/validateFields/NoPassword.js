import React, {Component} from 'react';

export default class NoPassword extends Component{
    render(){
        return(
            <div class="alert alert-danger" role="alert">
                Trebuie sa alegeti o parola de minim 6 caractere.
            </div>
        )
    }

}