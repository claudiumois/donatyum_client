import React, {Component} from 'react';

export default class NoMatchPassword extends Component{
    render(){
        return(
            <div class="alert alert-danger" role="alert">
                Nu ati confirmat parola.
            </div>
        )
    }

}