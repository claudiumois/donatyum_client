import React, { Component } from 'react';
import { Modal, Button, Row, Col, Form } from 'react-bootstrap';


class ModalConfirmPassword extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Confirmare Parola
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div className="col-xs-4">
                        <input type="password" className="form-control input-sm" onChange={this.props.confirmPass} value={this.props.value} />
                    </div>
                </Modal.Body>

                <Modal.Footer>
                    <Button id="button-style" variant="success" onClick={this.props.saveEditPassword}>Save</Button>
                    <Button id="button-style" variant="danger" onClick={this.props.onHide}>Inchide</Button>
                </Modal.Footer>
            </Modal>
        );
    }


}
export default ModalConfirmPassword
