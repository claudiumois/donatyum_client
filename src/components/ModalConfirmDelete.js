import React, { Component } from 'react';
import { Modal, Button, Row, Col, Form } from 'react-bootstrap';


class ModalConfirmDelete extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Confirmare Stergere
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div>
                        Esti sigur ca vrei sa stergi?
                    </div>
                </Modal.Body>

                <Modal.Footer>
                    <Button id="button-style" variant="success" onClick={this.props.removeStaff}>Da</Button>
                    <Button id="button-style" variant="danger" onClick={this.props.onHide}>Inchide</Button>
                </Modal.Footer>
            </Modal>
        );
    }


}
export default ModalConfirmDelete
