import React, {Component} from 'react';

export default class HasCreated extends Component{
    render(){
        return(
            <div class="alert alert-success" role="alert">
                Creare realizata cu sucess.
            </div>
        )
    }

}