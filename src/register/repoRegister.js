import axios from 'axios'

export const register = newUser => {
    return axios
        .post(`http://localhost:8080/api/customer/create`, newUser, {
            headers: { 'Content-Type': 'application/json' }
        })
        .then(response => {
            console.log(response)
        })
        .catch(err => {
            console.log(err)
        })
}