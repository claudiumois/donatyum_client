import React, { Component } from 'react'
import { register } from './repoRegister'
import { withRouter } from 'react-router-dom';
import DatePicker from "react-datepicker";
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button, Container } from 'reactstrap';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import "react-datepicker/dist/react-datepicker.css";
import '../commons/styles/backgroundStyle.css';
import './register.css';

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            firstname: '',
            lastname: '',
            email: '',
            phone: '',
            address: '',
            birthDate: new Date(),
            gender: '',
            open: false
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    handleClose = () => {
        this.setState({ open: false })
    };

    handleOpen = () => {
        this.setState({ open: true })
    };

    handleChange = (event) => {
        this.setState({ gender: event.target.value })
    };

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onChangeBirthDate = birthDate => this.setState({ birthDate })

    onSubmit(e) {
        e.preventDefault()

        const newUser = {
            userName: this.state.username,
            password: this.state.password,
            firstName: this.state.firstname,
            lastName: this.state.lastname,
            email: this.state.email,
            phone: this.state.phone,
            address: this.state.address,
            birthDate: this.state.birthDate,
            gender: this.state.gender,
        }

        console.log(newUser);

        const option = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newUser)
        };


        fetch('http://localhost:8080/api/customer/create', option)
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
            })
            .catch((error) => {
                console.error('Error:', error);
            });

        this.props.history.push('/login')
    }

    render() {
        return (

            <div class="back-color-register">
                <Card id="cardRegister">
                    <CardBody>
                        <CardTitle><h4>Creaza un cont nou</h4></CardTitle>
                        <div className="col-md-6 mt-5 mx-auto">
                            <form noValidate onSubmit={this.onSubmit}>

                                <div className="form-group">
                                    <label htmlFor="username">Username</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="username"
                                        placeholder="Enter your user name"
                                        value={this.state.username}
                                        onChange={this.onChange}
                                    />
                                </div>



                                <div className="form-group">
                                    <label htmlFor="password">Parola</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        name="password"
                                        placeholder="Password"
                                        value={this.state.password}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="firstname">Nume</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="firstname"
                                        placeholder="Enter your first name"
                                        value={this.state.firstname}
                                        onChange={this.onChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="lastname">Prenume</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="lastname"
                                        placeholder="Enter your lastname name"
                                        value={this.state.lastname}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="email">Adresa de mail</label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        name="email"
                                        placeholder="Enter email"
                                        value={this.state.email}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="address">Adresa</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="address"
                                        placeholder="Enter your address"
                                        value={this.state.address}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="phone">Numar de telefon</label>
                                    <input
                                        type="phone"
                                        className="form-control"
                                        name="phone"
                                        placeholder="Enter your phone"
                                        value={this.state.phone}
                                        onChange={this.onChange}
                                    />
                                </div>

                                <div className="form-group">
                                    <label htmlFor="birthDate">Zi de nastere</label>
                                    &nbsp;&nbsp;&nbsp;
                                    <DatePicker selected={this.state.birthDate} onChange={this.onChangeBirthDate} />
                                </div>

                                <InputLabel id="demo-controlled-open-select-label">Sex</InputLabel>
                                    <Select
                                        labelId="demo-controlled-open-select-label"
                                        id="demo-controlled-open-select"
                                        label="Birthdate"
                                        open={this.state.open}
                                        onClose={this.handleClose}
                                        onOpen={this.handleOpen}
                                        value={this.state.gender}
                                        onChange={this.handleChange}
                                    >
                                        <MenuItem value={"Masculin"}>Masculin</MenuItem>
                                        <MenuItem value={"Feminin"}>Feminin</MenuItem>
                                    </Select>
                                <p></p>
                                <div>
                                <button
                                    type="submit"
                                    class="button-register button1"
                                >
                                    <h5>Creaza</h5>
                                </button>
                                </div>
                                
                            </form>
                        </div>
                    </CardBody>
                </Card>
            </div>
        )
    }
}

export default withRouter(Register)
