import React, { useState } from 'react';
import './slider.scss'

import i1 from '../../commons/images/imageDonate/3.jpg'
import i2 from '../../commons/images/imageDonate/4.png'
import i3 from '../../commons/images/imageDonate/5.jpg'
import i4 from '../../commons/images/imageDonate/6.jpg'


function Slider() {
    let imgStyles={
        width: 100+"%",
        height: "auto"
    }

    let sliderArr = [
    <img src={i1} alt="slide-img" style={imgStyles} />,
    <img src={i2} alt="slide-img" style={imgStyles} />,
    <img src={i3} alt="slide-img" style={imgStyles} />,
    <img src={i4} alt="slide-img" style={imgStyles} />
    ];

    const [x, setX] = useState(0);

    const goLeft = () => {
        x === 0 ? setX(-100 * (sliderArr.length - 1)) : setX(x + 100);
    }

    const goRight = () => {
        (x === -100 * (sliderArr.length - 1)) ? setX(0) : setX(x - 100);
    }

    return (
        <div className="slider">
            {sliderArr.map((item, index) => {
                return (
                    <div key={index} className="slide" style={{ transform: `translateX(${x}%)` }}>
                        {item}
                    </div>
                )
            })}
            <button id="goLeft" onClick={goLeft}>
                <i class="fas fa-chevron-left"></i>
            </button>
            <button id="goRight" onClick={goRight}>
                <i class="fas fa-chevron-right"></i>
            </button>
        </div>
    )
}
export default Slider;