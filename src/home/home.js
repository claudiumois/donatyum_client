import React, { Component } from 'react';
import 'pure-react-carousel/dist/react-carousel.es.css';
import Slider from './Carousel/Slider';

class Home extends Component {
    render() {
        return (
            <div><Slider /></div>
        );
    }
};
export default Home