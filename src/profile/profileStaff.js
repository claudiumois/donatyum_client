import React, { Component } from 'react';
import axios from 'axios';
import './profile.css';
import { Card, CardImg, CardBody, CardTitle, Button, Table } from 'reactstrap';
import ModalEditStaff from 'react-modal';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import DatePicker from "react-datepicker";
import NoMatchPassword from '../components/NoMatchPassword';
import HasEditated from '../components/HasEditated';
import ModalConfirmPassword from '../components/ModalConfirmPassword';




import img from '../commons/images/1.jpg'
class ProfileStaff extends Component {
    constructor() {
        super()
        this.state = {
            name: '',
            idCustomer: '',
            firstName: '',
            lastName: '',
            userName: '',
            email: '',
            phone: '',
            address: '',
            birthDate: null,
            gender: '',
            password: '',

            isActiveEdit: false,
            errorMessages: '',
            successAction: '',
            showModalConfirmPassword: false, /*Confirm password modal */
            ifChangePassword: false,/*If change password with confirmation password in edit */
            showModalConfirmPassword: false, /*Confirm password modal */
        }
        this.handleSubmitEdit = this.handleSubmitEdit.bind(this);
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
        this.handleChangePhone = this.handleChangePhone.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangeBirthDate = this.handleChangeBirthDate.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
    }

    /* Pentru Edit. Atunci cand se face edit se face pe fiecare field imparte */

    handleChangeFirstName = (e) => {
        this.setState({
            firstName: e.target.value,
        });
    }

    handleChangeLastName = (e) => {
        this.setState({
            lastName: e.target.value,
        });
    }

    handleChangeUsername = (e) => {
        this.setState({
            userName: e.target.value,
        });
    }

    handleChangeEmail = (e) => {
        this.setState({
            email: e.target.value,
        });
    }

    handleChangeAddress = (e) => {
        this.setState({
            address: e.target.value,
        });
    }

    handleChangeUserOcupation = (e) => {
        this.setState({
            staffOcupationId: e.target.value,
        });
    }

    handleChangePhone = (e) => {
        this.setState({
            phone: e.target.value,
        });
    }

    handleChangeBirthDate = (e) => {
        this.setState({
            birthDate: e.target.value,
        });
    }


    handleChangePassword = (e) => {
        this.setState({
            password: e.target.value,

        });
    }

    handleChangeConfirmPassword = (e) => {
        this.setState({
            confirmPassword: e.target.value,
            ifChangePassword: true,
        });
    }

    handleChangeSalary = (e) => {
        this.setState({
            salary: e.target.value,
        });
    }

    handleCloseModalConfirmPassword = () => {
        this.setState({ showModalConfirmPassword: false })
    };

    handleOpenModalConfirmPassword = () => {
        this.setState({ showModalConfirmPassword: true })
    };

    /***************************************************************************/

    

    toggleModalEdit = () => {
        this.setState({
            isActiveEdit: !this.state.isActiveEdit
        })
    }

    componentWillMount() {
        ModalEditStaff.setAppElement('body');
    }

    componentDidMount() {
        const apiUrl = 'http://localhost:8080/api/staff/find/' + localStorage.getItem("userStorageId");

        console.log(apiUrl)
        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        name: result.firstName + ' ' + result.lastName,
                        firstName: result.firstName,
                        lastName: result.lastName,
                        userName: result.userName,
                        email: result.email,
                        phone: result.phone,
                        address: result.address,
                        birthDate: Date.parse(result.birthDate),
                        gender: result.gender,
                        password: result.password
                    });
                    console.log(result.gender)
                },
                (error) => {
                    this.setState({ error });
                }
            )

    }

    handleSubmitEdit = (e) => {
        e.preventDefault();

        const editStaff = {
            userName: this.state.userName,
            password: this.state.password,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            phone: this.state.phone,
            birthDate: this.state.birthDate,
            address: this.state.address,
            salary: this.state.salary,
            gender: this.state.gender,
            staffOcupationId: this.state.staffOcupationId
        }

        console.log('password=' + this.state.password);
        console.log(' confirm pass=' + this.state.confirmPassword)

        if (this.state.password != this.state.confirmPassword && this.state.ifChangePassword == true) {
            this.setState({
                errorMessages: 'passwordNotCorectly',
            })
        } else {
            this.setState({
                errorMessages: '',
                successAction: 'editated',
                showModalConfirmPassword: false,
            })
            const apiUrl = "http://localhost:8080/api/staff/update/" + localStorage.getItem("userStorageId");

            const options = {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(editStaff)
            }

            fetch(apiUrl, options)
                .then(this.setState({
                    successAction: 'editated'
                }))
        }


    }

    roleStaff = (ocupationId) => {

        if (ocupationId == 1) {
            return "Admin";
        } else if (ocupationId == 2) {
            return "Doctor";
        } else if (ocupationId == 3) {
            return "Asistent"
        }
    }

    closeAndSaveEdit = () => {
        this.toggleModalEdit();
        window.location.reload(false);
    }


    render() {


        return (

            <div class="back-color">
                <Card id="card-profile">
                    <CardBody>
                        <CardTitle id="title">Profil</CardTitle>
                        <CardImg >
                        {/* <Avatar alt="Remy Sharp" src={'http://localhost:8000/api/store_image/fetch_image/'} /> */}
                        </CardImg>
                        <CardBody>
                            <Table>
                                <thead>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Nume</th>
                                        <td>{this.state.firstName} {this.state.lastName}</td>
                                    </tr>

                                    <tr>
                                        <th>Email</th>
                                        <td>{this.state.email}</td>
                                    </tr>

                                    <tr>
                                        <th>Adresa</th>
                                        <td>{this.state.address}</td>
                                    </tr>

                                    

                                    <tr>
                                        <th>Nr. Telefon</th>
                                        <td>0{this.state.phone}</td>
                                    </tr>

                                </tbody>

                                <Button id="button-style" onClick={this.toggleModalEdit} variant="success" class="success">Setari</Button>


                            </Table>
                        </CardBody>
                    </CardBody>
                </Card>

                <ModalEditStaff
                    className="white"
                    size="modal-body"
                    overlayClassName="Overlay"
                    aria-labelledby="example-modal-sizes-title-sm" isOpen={this.state.isActiveEdit} onRequestClose={this.toggleModalEdit}>
                    <Button id="button-style" type="submit" onClick={this.closeAndSaveEdit} className="btn btn-secondary">Salveaza si inchide</Button>


                    <div>
                        <div className="col-sm-4 mx-auto">
                            <h1 className="text-center">Editare Cont</h1>
                        </div>


                        <table id="students">
                            <thead>

                            </thead>
                            <tbody>
                                <tr>
                                    <th>Nume</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>

                                            <div className="col">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.firstName} onChange={this.handleChangeFirstName} value={this.state.firstName} />
                                            </div>

                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Prenume</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div className="col-xs-4">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.lastName} onChange={this.handleChangeLastName} value={this.state.lastName} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Username</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div className="col-xs-4">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.userName} onChange={this.handleChangeUsername} value={this.state.userName} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>


                                <tr>
                                    <th>Email</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div className="col-xs-4">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.email} onChange={this.handleChangeEmail} value={this.state.email} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        Parola
                                    </th>
                                    <td><form onSubmit={this.handleSubmitEdit}>
                                        {this.state.errorMessages == "passwordNotCorectly" ? <NoMatchPassword /> : null}
                                        <div className="col-xs-4">
                                            <input type="password" className="form-control input-sm" placeholder={this.state.password} onChange={this.handleChangePassword} value={this.state.password} />
                                        </div>
                                        
                                        <Button id="button-style" onClick={this.handleOpenModalConfirmPassword} className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                    </form></td>
                                </tr>

                                <tr>
                                    <th>Numar de telefon</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div class="col-xs-4">
                                                <input type="text" className="form-control" id="ex3" placeholder={this.state.phone} onChange={this.handleChangePhone} value={this.state.phone} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>

                                        </form>
                                    </td>
                                </tr>


                                <tr>
                                    <th>Adresa</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" id="ex3" placeholder={this.state.address} onChange={this.handleChangeAddress} value={this.state.address} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                               


                                <tr>
                                    <th>Zi de nastere</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>

                                            <div>
                                                <DatePicker {...this.state.birthDate} placeholderText={this.state.birthDate} selected={this.state.birthDate} onChange={this.handleChangeBirthDate} />
                                            </div>
                                            <Button id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>


                                    </td>
                                </tr>

                                <tr>
                                    <th>Salariu</th>
                                    <td>
                                        <form onSubmit={this.handleSubmitEdit}>
                                            <div className="col-xs-4">
                                                <input type="text" className="form-control input-sm" placeholder={this.state.salary} onChange={this.handleChangeSalary} value={this.state.salary} />
                                            </div>
                                            <Button id="button-style" id="button-style" type="submit" className="btn btn-info"><i class="fas fa-user-edit"></i></Button>
                                        </form>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Sex</th>
                                    <td>{this.state.gender}</td>
                                </tr>

                            </tbody>

                        </table>

                        <p></p>
                        <p></p>
                        <p></p>

                    </div>

                </ModalEditStaff>

                <ModalConfirmPassword
                    show={this.state.showModalConfirmPassword}
                    onHide={this.handleCloseModalConfirmPassword}
                    saveEditPassword={this.handleSubmitEdit}
                    confirmPass={this.handleChangeConfirmPassword}
                >

                </ModalConfirmPassword>
            </div>


        )
    }
}

export default ProfileStaff