import React, { Component } from 'react'
import './nav.css';
import { Link, withRouter } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';


class NavigationBarRegLink extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            nrAppointmentNoChecked: '',
        }
    }

    logOut(e) {
        e.preventDefault()
        localStorage.removeItem('userStorageId');
        localStorage.removeItem('userOcupationId');
        localStorage.removeItem('completeQuestionnaire');
        this.props.history.push(`/login`);

    }

    //http://localhost:8080/api/customer/find/18

    componentDidMount() {
        const apiUrl = 'http://localhost:8080/api/customer/find/' + localStorage.getItem("userStorageId");

        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        username: result.userName,
                    })
                },
                (error) => {
                    this.setState({ error });
                }
            )

        const apiAllAppointmentNoChecked = 'http://localhost:8080/api/appointment/findAllAppointmentsNoChecked';
        fetch(apiAllAppointmentNoChecked)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        nrAppointmentNoChecked: result.length,
                    })
                    console.log('nr de programari neverificate = ' + result.length);
                },
                (error) => {
                    this.setState({ error });
                }
            )
        
    }

    render() {


        const loginRegLink = (

            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="/">Acasa</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/donationGuide">Ghid Donare</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/aboutUs">Despre noi</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/contact">Contact</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Autentificare</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/register">Inregistrare</a>
                        </li>
                    </ul>

                </div>
            </div>
        )

        const userLink = (

            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="/">Acasa</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/donationGuide">Ghid donare</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/aboutUs">Despre noi</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/contact">Contact</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/appointment">Programare</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/q">Chestionar</a>
                        </li>
                    </ul>



                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="/profile" onClick={this.hiProfile}><i class="fas fa-user-alt"> {this.state.username}</i></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="" onClick={this.logOut.bind(this)}>Deconectare</a>
                        </li>
                    </ul>
                </div>
            </div >
        )

        const adminLink = (

            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="/appointmentAdmin"><i class="fas fa-calendar-check"> </i> Programari</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/staff"><i class="fas fa-users"></i> Personal</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/donorAdmin"><i class="fas fa-hand-holding-medical"></i> Donatori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/alldoctor"><i class="fas fa-user-md"></i> Doctori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/alladmin"><i class="fas fa-user-shield"></i> Administratori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/allassistent"><i class="fas fa-user-nurse"></i> Asistenti</a>
                        </li>
                    </ul>



                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="/staffProfile" ><i class="fas fa-user-alt"> {this.state.username}</i></a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" href="" onClick={this.logOut.bind(this)}>Deconectare</a>
                        </li>
                    </ul>
                </div>
            </div >
        )



        const doctorLink = (

            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="/appointmentDoctor"><i class="fas fa-calendar-check"> {this.state.nrAppointmentNoChecked} Cereri Programari</i></a>
                        </li>

                        
                        <li class="nav-item">
                            <a class="nav-link" href="/appointmentAcceptedDoctor"><i class="fas fa-calendar-check"> Programari acceptate</i></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/bloodStore"><i class="fas fa-sort-amount-up"> Stoc sange</i></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/donorDoctor"><i class="fas fa-hand-holding-medical"></i> Donatori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/alldoctor"><i class="fas fa-user-md"></i> Doctori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/alladmin"><i class="fas fa-user-shield"></i> Administratori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/allassistent"><i class="fas fa-user-nurse"></i> Asistenti</a>
                        </li>
                    </ul>



                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="/staffProfile" ><i class="fas fa-user-alt"> {this.state.username}</i></a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" href="" onClick={this.logOut.bind(this)}>Deconectare</a>
                        </li>
                    </ul>
                </div>
            </div >
        )

        const asistentLink = (
            <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul class="navbar-nav mr-auto">

                        {/* <li class="nav-item">
                            <a class="nav-link" href="/appointmentDoctor"><i class="fas fa-calendar-check"> {this.state.nrAppointmentNoChecked} Programari</i></a>
                        </li> */}

                        <li class="nav-item">
                            <a class="nav-link" href="/appointmentAssistent"><i class="fas fa-calendar-check"> Programari</i></a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/donorDoctor"><i class="fas fa-hand-holding-medical"></i> Donatori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/alldoctor"><i class="fas fa-user-md"></i> Doctori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/alladmin"><i class="fas fa-user-shield"></i> Administratori</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/allassistent"><i class="fas fa-user-nurse"></i> Asistenti</a>
                        </li>
                    </ul>



                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="/staffProfile" ><i class="fas fa-user-alt"> {this.state.username}</i></a>
                        </li>


                        <li class="nav-item">
                            <a class="nav-link" href="" onClick={this.logOut.bind(this)}>Deconectare</a>
                        </li>
                    </ul>
                </div>
            </div >
        )



        const project = () => {

            if (localStorage.getItem('userOcupationId') == 0 && localStorage.getItem('userOcupationId') != null) {
                return userLink;
            } else if (localStorage.getItem('userOcupationId') == 1) {
                return adminLink;
            } else if (localStorage.getItem('userOcupationId') == 2) {
                return doctorLink;
            } else if(localStorage.getItem('userOcupationId') == 3) {
                return asistentLink;
            }else {
                return loginRegLink;
            }
        }

        return (
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark rounded">
                <div id="navbar1" className="collapse navbar-collapse justify-content-md-center">
                    <ul className="navbar-nav">
                        <li className="nav-item">

                        </li>
                    </ul>
                    {project()}
                </div>

            </nav>

        )
    }


}
export default withRouter(NavigationBarRegLink) 
