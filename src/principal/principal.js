import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import NavigationBarRegLink from './navigation-bar'
import Home from '../home/home';
import DonationGuide from '../donationGuide/donationGuide';
import Contact from '../contact/contact';
import AboutUs from '../aboutUs/aboutUs';
import LogIn from '../login/login';
import Register from '../register/register';
import Appointment from '../appointment/appointment';
import Profile from '../profile/profile';
import Q from '../questionnaire/questionnaire';

/***Staff***/
import StaffProfile from '../profile/profileStaff';
/***********/

/************ADMIN******************/
import Staff from '../admin/staff/staff';
import Donor from '../admin/donor/donor';
import AppointmentAdmin from '../admin/appointment/appointment';
import Doctors from '../admin/staff/doctor/Doctors';
import Admins from '../admin/staff/admin/Admins';
import Assistents from '../admin/staff/assistent/Asistents';
/***********************************/

/***************DOCTOR**************/
import AppointmentDoctor from '../doctor/appointment/appointment';
import AppointmentAcceptedDoctor from '../doctor/appointment/appointmentAccepted';
import DonorDoctor from '../doctor/donor/donor';
import BloodDoctor from '../doctor/blood/blood';
/***********************************/

/***************ASISTENT**************/
import AppointmentAsistent from '../assistent/appointment/appointment';
/*************************************/

import ErrorPage from '../commons/errorhandling/error-page';


function Principal() {

    return (
        <div>
            <Router>
                <div>
                    <NavigationBarRegLink />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home />}
                        />

                        <Route
                            exact
                            path='/donationGuide'
                            render={() => <DonationGuide />}
                        />

                        <Route
                            exact
                            path='/contact'
                            render={() => <Contact />}
                        />

                        <Route
                            exact
                            path='/aboutUs'
                            render={() => <AboutUs />}
                        />

                        <Route
                            exact
                            path='/login'
                            render={() => <LogIn />}
                        />

                        <Route
                            exact
                            path='/register'
                            render={() => <Register />}
                        />

                        <Route
                            exact
                            path='/appointment'
                            render={() => <Appointment />}
                        />

                        <Route
                            exact
                            path='/profile'
                            render={() => <Profile />}
                        />

                        <Route
                            exact
                            path='/staffProfile'
                            render={() => <StaffProfile />}
                        />

                        <Route
                            exact
                            path='/q'
                            render={() => <Q />}
                        />

                        /*Admin */
                        <Route
                            exact
                            path='/staff'
                            render={() => <Staff />}
                        />

                        <Route
                            exact
                            path='/donorAdmin'
                            render={() => <Donor />}
                        />

                        <Route
                            exact
                            path='/appointmentAdmin'
                            render={() => <AppointmentAdmin />}
                        />

                        <Route
                            exact
                            path='/alldoctor'
                            render={() => <Doctors />}
                        />

                        <Route
                            exact
                            path='/alladmin'
                            render={() => <Admins />}
                        />

                        <Route
                            exact
                            path='/allassistent'
                            render={() => <Assistents />}
                        />
                        /********/

                        /*****DOCTOR*****/
                        <Route
                            exact
                            path='/appointmentDoctor'
                            render={() => <AppointmentDoctor />}
                        />

                        <Route
                            exact
                            path='/donorDoctor'
                            render={() => <DonorDoctor />}
                        />

                        <Route
                            exact
                            path='/appointmentAcceptedDoctor'
                            render={() => <AppointmentAcceptedDoctor />}
                        />

                        <Route
                            exact
                            path='/bloodStore'
                            render={() => <BloodDoctor />}
                        />                     
                        /****************/

                        /****ASISTENT****/
                        <Route
                            exact
                            path='/appointmentAssistent'
                            render={() => <AppointmentAsistent />}
                        />
                        /****************/

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage />}
                        />

                        <Route render={() => <ErrorPage />} />
                    </Switch>
                </div>
            </Router>
        </div>
    )

}

export default Principal
