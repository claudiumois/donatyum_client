import React from 'react';
import { withRouter } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Button } from 'react-bootstrap';
import Moment from 'moment';

import TimePicker2 from 'react-timepicker-c';

import './appointment.css'

class Appoiment extends React.Component {

    constructor() {
        super()
        this.state = {
            date: null,
            time: '',
            appointment: [],
            customer: [],
            name: '',
            errors: {},
            timeValue: '',
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    handleTimeChange = (time) => {
        this.setState({
            time
        })
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onChangeDate = date => this.setState({ date })
    onChangeTime = time => this.setState({ time })

    componentDidMount() {/*GET Appointment CUSTOMER*/
        const apiUrl = 'http://localhost:8080/api/appointment/getAppointmentByCustomerId/' + localStorage.getItem('userStorageId');

        console.log(apiUrl);
        fetch(apiUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        appointment: result,
                    });

                },
                (error) => {
                    this.setState({ error });
                }
            )

        const apigetcustomer = 'http://localhost:8080/api/customer/find/' + localStorage.getItem('userStorageId');

        console.log(apigetcustomer);
        fetch(apigetcustomer)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        name: result.firstName + " " + result.lastName
                    });

                },
                (error) => {
                    this.setState({ error });
                }
            )
    }

    onSubmit(e) {
        e.preventDefault()


        /*Creare programare */
        var oramin = this.state.time[0]+this.state.time[1]+this.state.time[2]+this.state.time[3]+this.state.time[4];
        var timp = oramin + ':00'
        const appointment = {
            idCustomer: localStorage.getItem("userStorageId"),
            date: this.state.date,
            status: 0,
            time: timp,
        }

        console.log(appointment)

        const options = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        var date = Moment(appointment.date).format('YYYY-MM-DD')
        const apiUrl = 'http://localhost:8080/api/appointment/setAppointmentToCustomer/'
            + appointment.idCustomer + "/" + date + "/" + appointment.time;

        console.log(apiUrl);

        fetch(apiUrl, options)
            .then(res => res.json())
            .then(res => console.log(res))

        /***************************************/



        /************Notifica toti doctori cu privire la programarea respectiva****************/

        const optionsForSenderMail = {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }
        var date = Moment(appointment.date).format('YYYY-MM-DD')
        const apiUrlSenderMail = 'http://localhost:8080/sendMailToDoctors/'
            + this.state.name + "/" + date + "/" + appointment.time;

        console.log(apiUrlSenderMail);

        fetch(apiUrlSenderMail, optionsForSenderMail)
            .then(res => res.json())
            .then(res => console.log(res))

        /******************************************************************************************/

       window.location.reload(false);
    }

    deleteAppointment(id) {/*DELETE Appointment CUSTOMER WITH ID*/
        const { appointment } = this.state;

        const apiUrl = 'http://localhost:8080/api/appointment/delete/' + id;
        const formData = new FormData();
        formData.append('id', id);

        const options = {
            method: 'DELETE',
            body: formData
        }

        fetch(apiUrl, options)
            .then(res => res.json())

        window.location.reload(false);
    }

    showAppointment = () => {
        const { appointment } = this.state;
        if (appointment.id != '') {
            return (
                <div>
                    <hr />
                    <h5 id='title'>Programarea ta</h5>
                    <table id='students'>
                        <thead>
                            <tr>
                                <th>Data</th>
                                <th>Ora</th>
                                <th>Optiune</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr key={appointment.id}>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>
                                    <Button variant="danger" onClick={() => { this.deleteAppointment(appointment.id) }}>Delete</Button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            );
        } else {
            return (
                <div></div>
            );
        }
    }

    render() {

        return (
            <div >
                <div className="back-color">
                    <hr />
                    <div className="col-sm-8 col-sm-offset-2">
                        <div className="panel panel-primary">
                            <div className="panel-heading">
                                <h3>Pentru a te programa, selectati data si ora</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={this.onSubmit}>
                                    <div>
                                        <h5>Data </h5>
                                        <DatePicker selected={this.state.date} onChange={this.onChangeDate} />
                                    </div>
                                    <p></p>
                                    {/* <TimePicker selected={this.state.time} onChange={this.onChangeTime} /> */}
                                    <p></p>
                                    <div
                                        style={{
                                            width: "200px",
                                            height: "30px",
                                        }}
                                    >
                                        <label><h5>Ora </h5></label>
                                        <TimePicker2
                                            minTime='10:00am'
                                            maxTime='12:30pm'
                                            onChange={this.onChangeTime}
                                            timeValue={this.state.time}
                                            name="timeField"
                                            interval="10"
                                        />
                                    </div>
                                    <p></p>
                                    <br/>
                                    <Button type="submit" id="button-style" variant="success">Creaza programare</Button>
                                </form>
                            </div>


                        </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    {this.showAppointment()}
                </div>
            </div>
        );
    }
}
export default withRouter(Appoiment)