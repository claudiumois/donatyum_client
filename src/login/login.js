import React from 'react';
import { login } from './repoLogin';
import { withRouter } from 'react-router-dom';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button, Container } from 'reactstrap';

import './login.css';
import '../commons/styles/backgroundStyle.css';

class Login extends React.Component {

  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      errors: {}
    }

    this.onChange = this.onChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onChange(e) {

    this.setState({ [e.target.name]: e.target.value })


  }

  onSubmit(e) {
    e.preventDefault()

    const user = {
      email: this.state.email,
      password: this.state.password
    }



    login(user).then(res => {
      this.props.history.push('/')
    })

  }

  render() {
    return (
      
      <div class="back-color">
      <Card id="card">
        <CardBody>
          <CardTitle id="title">Autentificare</CardTitle>
          <div className="panel-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label><h6>Email</h6></label>
                <input type="text" className="form-control" name="email" value={this.state.email}
                  onChange={this.onChange} />
              </div>
              <div className="form-group">
                <label><h6>Parola</h6></label>
                <input type="password" className="form-control" name="password" value={this.state.password} onChange={this.onChange} />
              </div>
              <button type="submit" class="button-login button1">Autentificare</button>
            </form>
          </div>
        </CardBody>
      </Card>
      </div>
    );
  }
}
export default withRouter(Login)