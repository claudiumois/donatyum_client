import axios from 'axios';


export const login = user => {
    return axios
        .post(`http://localhost:8080/api/customer/login`, 
         {
                email: user.email,
                password: user.password
        }, 

             {
			headers: {'Content-Type': 'application/json'},
			 },
		)
		.then(response => 
            {
            
            localStorage.setItem('userStorageId', response.data.userId);
            localStorage.setItem('userOcupationId', response.data.userOcupationId);
            
            console.log(response.data);

            return response.data
            
        })
        .catch(err =>  alert("Authentification failed!"))
}

