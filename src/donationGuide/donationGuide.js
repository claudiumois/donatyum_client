import React from 'react';
import '../commons/styles/backgroundStyle.css';


function DonationGuide() {

    return (

        <div className="back-color">
            
            <h5>
                <p>
                Dacă doriți să donați  sânge trebuie să aflați că este foarte simplu și sigur să faceți acest lucru. 
                </p>

                <p>
                Donarea de sânge este un act voluntar, anonim și neremunerat. Donatorul de sânge este orice cetățean român cu domiciliul în România sau orice cetățean al Uniunii Europene care are reședința în România, în deplină stare de sănătate fizică și mentală, care acceptă regulile prelevării de sânge total și componente sanguine de origine umană. 
                </p>
                <p>Pentru a deveni donator de sânge trebuie să îndepliniți următoarele condiții: </p>
                <p>
                    - orice persoană care dorește să doneze sânge trebuie să se prezinte, pentru identificare, cu un document (ex. buletin, CI, carnet de conducere sau pașaport, etc.); 
                </p>
                <p>
                    - să aibă vârsta cuprinsă în intervalul 18 – 60 ani (la prima donare, peste 50, ani numai cu acordul medicului de la triaj); 
                </p>
                <p>
                    - greutate 50-100 kg femeile, 60-110 kg bărbații;
                </p>
                <p>
                    - tensiunea arterială:  60 – 100 mm Hg cu 100 - 180 mm Hg; 
                </p>
                <p>
                    - pulsul regulat, 50-100 bătăi-minut;
                </p>
                <p>
                    - hemoglobina în intervalul 12.5-17.5 g/dl femeile, 13.5 – 19.5 g/dl bărbații;
                </p>
                <p>
                    - să nu fi suferit în ultimele luni intervenții chirurgicale, tatuaje;
                </p>
                <p>
                    - femeile să nu fie însărcinate, in perioada de alăptare, lăuzie, în perioada menstruală (la 5 zile după menstruație se poate dona);
                </p>
                <p>
                    - să nu prezinte afecțiuni cardiovasculare, afecțiuni ale sistemului nervos central, tendință patologică la sângerare, afecțiuni hematologice, renale, metabolice (diabet zaharat) și endocrine, reumatologice și imunologice, dermatologice, boli profesionale, boli infecțioase (hepatite virale, etc.), parazitare , afecțiuni maligne, TBC, ulcer, persoane pensionate medical
                </p>
                <p>
                    - să nu fie analfabet (pentru a putea completa chestionarul de autoexcludere, pentru  citi drepturile și obligațiile ce îi revin ca donator de sânge);
                </p>
                <p>
                    - să nu fi consumat grăsimi, alcool în ultimele 48 de ore; etc. 
                </p>
                <p>
                    - să fie odihnit.
                </p>
                <p></p>
                <br />
                <p>
                    Înainte de a dona: 
                </p>
                <p>
                    - dimineața înainte de donare să serviți un mic dejun  (fără grăsimi), să vă hidratați; 
                </p>
                <p>
                    - să nu fumați înainte și după donare cel puțin o oră. 
                </p>
                <br />
                <p>
                    Etapele donării:
                </p>
                <p>
                    - completarea chestionarului de către donator; 
                </p>
                <p>
                    - întocmirea fișei donatorului; 
                </p>
                <p>
                    - examenul medical al donatorului; 
                </p>
                <p>
                    - măsurarea tensiunii arteriale, a pulsului;
                </p>
                <p>
                    - controlul biologic înainte de donare (grupă sanguină, hemoglobină, glicemie) 
                </p>
                <p>
                    - recoltarea de sânge;
                </p>
                <p>
                    - repaus post-donare obligatoriu timp de 10 minute și compresie la locul puncției. 
                </p>
                <p>
                    - La o donare de sânge se recoltează 450 ml sânge, cantitate care se reface ca volum în câteva ore, celular în doua săptămâni.
                </p>
                <p>
                    - Trusa de recoltare este sterilă și de unică folosință; NU există riscul de contaminare; - În primele 24  de ore după donare este recomandat să se crească consumul de lichide și proteine. 
                </p>
                <p>
                    - Numărul de donări/an: pentru bărbați 4-5 ori/an (minim 70 de zile între 2 donări standard), pentru femeie 3-4 donări/an (minim 90 de zile între 2 donări standard).
                </p>
                <p>
                    - Dacă apar sângerări sau vânătăi sub piele, aplicați comprese reci pe locul respectiv. 
                </p>
                <p>
                    - Rezultatele testelor sunt confidențiale.
                </p>
                <p>
                    - Sunteți informați în ceea ce privește rezultatele patologice. 
                </p>
                <br />
                <p>
                    - Sângele donat de dumneavoastră poate contribui la salvarea vieții unui copil, unei mame, unui tată, unui frate sau surori.  
                </p>
                <p>
                    - O doare de sânge poate salva 3 vieți! 
                </p>
                <p>
                    - Sângele donat de dumneavoastră va fi întotdeauna necesar și poate fi de folos cuiva.
                </p>
                <p>
                    - Procedura de donare este simplă, rapidă și inofensivă. 
                </p>
                <p>
                    - Este un lucru demn de urmat, uman, corect, care nu cere un efort mare pentru a-l face. 
                </p>
                <p>
                    - E un mod de a fi admirat de cei din jur pentru gestul care l-ai făcut.
                </p>
                <p>
                    - E un mod de a arăta că ești receptiv la suferința altor oameni. 
                </p>
                <br />
                <p>
                    Beneficiile donării de sânge:
                </p>
                <p>
                    Efectuarea testelor de laborator din sângele recoltat: grupa sanguină, Rh D, hepatită B, C, HIV, HTLV, ALT, lues, etc. și eliberarea Buletinelor de analiză la cerere; 
                </p>
                <p>
                    După donare, sângele este împrospătat, imunitatea organismului crește; riscul de accident vascular și de paralizie se reduc cu până la 30%; 
                </p>
                <p>
                    La fiecare donare se pierd 650 de calorii;
                </p>
                <p>
                    Monitorizarea stării de sănătate; 
                </p>
                <p>
                    Adeverință pentru 1 zi liberă de la locul de muncă, sau o zi scutire pentru elevi, studenți și militari în ziua donării;
                </p>
                <p>
                    Adeverință pentru reducere 50% la abonamentul pentru transportul în comun (pentru 30 de zile) pentru persoanele care donează la Centrul de Transfuzie Sanguină MM, cu domiciliul în Baia Mare;
                </p>
                <p>
                    Se acordă 7 tichete de masă. 
                </p>
                <br />
                <p>
                    Gestul donării de sânge completează personalitatea fiecăruia dintre noi, dovedind empatia, dăruirea și dragostea! 
                </p>
                <p>
                    Sângele reprezintă un dar neprețuit, care nu poate fi cumpărat, ci poate fi donat de la o persoană la alta. Este ceva ce poți oferi ,,din inimă,, ! 
                </p>
                <p>
                    Vă așteptăm la donare, să dăm șansă vieții!!! 
                </p>

            </h5>


        </div>
    )
}

export default DonationGuide
