import React from 'react';

function Progress(props) {
    return (
        <h2>
            Intrebarea {props.current} din {props.total}
        </h2>
    );
}
export default Progress;