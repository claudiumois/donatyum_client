import React from 'react';
import Answer from './Answer';

function Answers(props) {
    return (
        <>
            <Answer letter="a"
                answer={props.question.answer_a}
                handleClick={props.handleClick}
                selected={props.currentAnswer === 'a'}
            />
            <Answer
                letter="b"
                answer={props.question.answer_b}
                handleClick={props.handleClick}
                selected={props.currentAnswer === 'b'}
            />

        </>
    );
}
export default Answers;