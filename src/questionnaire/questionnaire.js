import React, { useState, useEffect } from "react";
import { withRouter } from 'react-router-dom';
import Question from './Question';
import Progress from './Progress';
import Answers from './Answers';

import './q.css'

function Q() {

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [currentAnswer, setCurrentAnswer] = useState('');
    const [answers, setAnswers] = useState([]);
    const [showResults, setShowResults] = useState(false);
    const [error, setError] = useState('');

    const questions = [
        {
            id: 1,
            question: 'Considerati ca aveti o stare buna de sanatate',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 2,
            question: 'In ultima vreme ati avut o pierdere in greutate neasteptata',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 3,
            question: 'In ultima vreme ati avut febra neexplicabila',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 4,
            question: 'In ultima vreme ati avut un tratament stomatologic, vaccinari',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 5,
            question: 'Urmariti vreun tratament medicamentos',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 6,
            question: 'In ultimele 12 luni, ati avut contact sexual cu un partener cu hepatita sau HIV pozitiv',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 7,
            question: 'In ultimele 12 luni, ati avut contact sexual cu un partener ce se drogheaza prin injectii',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 8,
            question: 'In ultimele 12 luni, ati avut contact sexual cu un partener care este plătit pentru sex',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 9,
            question: 'In ultimele 12 luni, ati avut contact sexual cu un partener cu hemofilie',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 10,
            question: 'In ultimele 12 luni, ati avut contact sexual cu parteneri multipli',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 11,
            question: 'V-aţi injectat vreodată droguri',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 12,
            question: 'Aţi acceptat bani sau droguri pentru a întreţine relaţii sexuale',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 13,
            question: 'Ca si barbat, sau ca fiemeie, barbatul dumneavoastra sau dumneavoastra ca si barbat, ati avut relatii sexuale cu un alt barbat',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 14,
            question: 'Ati schimbat partenerul (partenera) în ultimele 6 luni',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 15,
            question: 'De la ultima donare, sau în ultimele 12 luni aţi suferit o intervenţie chirurgicală sau investigaţii medicale',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 16,
            question: 'De la ultima donare, sau în ultimele 12 luni aţi suferit tatuaje, acupunctură, găuri pentru cercei',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 17,
            question: 'De la ultima donare, sau în ultimele 12 luni aţi fost transfuzat (a) ',
            answer_a: 'DA',
            answer_b: 'NU',
        },/*Mai trebe sa adaug niste intrebari cu alte raspunsuri */
        {
            id: 18,
            question: 'V-aţi născut, aţi trăit sau călătorit în străinătate',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 19,
            question: 'Aţi fost în detenţie (INCHISOARE)  în ultimul an',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 20,
            question: 'Aţi fost expuşi la hepatită(bolnavi în familie sau risc profesional)',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 21,
            question: 'Ati suferit vreodata de icter, tuberculoză, febră reumatică, malarie',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 22,
            question: 'Ati suferit vreodata de boli de inimă, tensiune arterială mare sau mică',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 23,
            question: 'Ati suferit vreodata de boli transmise sexual (hiv, sifilis etc) ',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 24,
            question: 'Ati suferit vreodata de accidente vasculare cardiace sau cerebrale',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 25,
            question: 'Ati suferit vreodata de convulsii, boli nervoase',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 26,
            question: 'Ati suferit vreodata de boli cronice (diabet, ulcer, cancer, astm) ',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 27,
            question: 'Sunteţi fumător',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 28,
            question: 'Aţi consumat alcool recent ',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 29,
            question: 'Aţi fost refuzat sau amânat la o donare anterioară ',
            answer_a: 'DA',
            answer_b: 'NU',
        },
        {
            id: 30,
            question: ' Aveţi pasiuni sau ocupaţii ce necesită atenţie specială 24 ore  postdonare (de ex: sofer, alpinist, scafandru, etc) ',
            answer_a: 'DA',
            answer_b: 'NU',
        },
    ];

    const question = questions[currentQuestion];


    useEffect(() => {/*VERIFICA DACA CLIENTUL A COMPLETAT CHESTIONARUL*/
        fetch('http://localhost:8080/api/takeq/checkifexistcompleteq/' + localStorage.getItem('userStorageId'))
            .then(res => res.json())
            .then((data) => {
                console.log(data.status);
                if (data.status == 1) {
                    localStorage.setItem('completeQuestionnaire', 1)
                    console.log('status =' + data.status)
                } else {
                    localStorage.setItem('completeQuestionnaire', 0)
                    console.log('status =' + data.status)
                }

            })
            
    });

    const handleClick = e => {
        setCurrentAnswer(e.target.value);
        setError('');
    }

    const renderError = () => {
        if (!error) {
            return;
        }

        return <div className="error">{error}</div>
    };

    const next = () => {
        const answer = { questionId: question.id, answer: currentAnswer, question: question.question, answer_a: question.answer_a, answer_b: question.answer_b, };

        if (!currentAnswer) {
            setError('Alege-ti o optiune');
            return;
        }

        answers.push(answer);

        setAnswers(answers);
        setCurrentAnswer('');

        if (currentQuestion + 1 < questions.length) {
            setCurrentQuestion(currentQuestion + 1);
            return;
        }

        setShowResults(true);
    }

    const sendAnswer = () => {/*Aici se face fetch(cand apasam confirma si continua la urmatoarea intrebare, se adauga in baza de date un nou chestionar) */

        var i = 0;
        answers.map(answer => {
            const newQuestionaire = {
                question: answer.question,
                answer_a: answer.answer_a,
                answer_b: answer.answer_b,
                answerCustomer: answer.answer,
                customerId: localStorage.getItem('userStorageId')
            }

            console.log(newQuestionaire);

            const option = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                // body: JSON.stringify(newQuestionaire)
            };

            var apiUrl = "http://localhost:8080/api/q/setQToCustomer/"+ newQuestionaire.customerId + "/" + newQuestionaire.question + 

            "/" + newQuestionaire.answer_a + "/" + newQuestionaire.answer_b + "/" + newQuestionaire.answerCustomer

            fetch(apiUrl, option)
                .then(response => response.json())
                .then(data => {
                    console.log('Success:', data);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
        })

        //http://localhost:8080/api/takeq/create

        /* Imi seteaza in baza de date in tabela TakeQ daca clientul a facut chestionarul*/
        const newQuestionaire = {
            status: '1',
            customerId: localStorage.getItem('userStorageId')
        }

        console.log(newQuestionaire);

        const option = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newQuestionaire)
        };


        fetch('http://localhost:8080/api/takeq/create', option)
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
            })
            .catch((error) => {
                console.error('Error:', error);
            });

        /************************************************************/

        //this.props.history.push("/appointment");
        window.location.reload(false);
    }

    if (showResults) {
        return (
            <div className="container results">
                <button class="button-login button1" onClick={() => {sendAnswer()}}>Trimite</button>
            </div>
        )
    } else {

        if (localStorage.getItem('completeQuestionnaire') != 1 && localStorage.getItem('completeQuestionnaire') != null) {
            return (

                <div className="container">
                    <Progress total={questions.length} current={currentQuestion + 1} />
                    <Question question={question.question} />
                    {renderError()}
                    <Answers
                        question={question}
                        currentAnswer={currentAnswer}
                        handleClick={handleClick}
                    />
                    <button className="btn btn-primary" onClick={next}>
                        Confirma
                    </button>
                </div>
            );
            
        } else {
            return (
                <div className="container">Ati completat chestionarul, puteti sa va programati pentru donare!</div>
            )
        }
    }
}

export default withRouter(Q);